import React, { Suspense } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import Home from './Home';
import Search from './Search';
import Group from './Group';
import Section from './Section';
import Page from './Page';
import ToTop from './ToTop';
import data from '../data';

import styles from './Main.module.css'

export default function Main() {
    return <main>
        <div className={ styles.wrapper }>
            <Suspense fallback={ null }>
                <Switch>
                    <Route exact strict path="/">
                        <Home />
                    </Route>
                    <Route exact strict path="/search">
                        <Search />
                    </Route>
                    
                    { Object.values(data.groups).filter((group) => !group.noIndex).map((group) => 
                        <Route exact path={ `/group/${ group.id }` } key={ `/group/${ group.id }` }>
                            <Group group={ group } />
                        </Route>
                    ) }

                    { Object.values(data.groups).filter((group) => !group.noIndex && group.noSection).map((group) => group.pages.map((page) => 
                        <Route exact path={ `/group/${ group.id }/${ page.id }` } key={ `/group/${ group.id }/${ page.id }` }>
                            <Page page={ page } />
                        </Route>
                    )) }

                    { data.sections.filter((section) => !section.disabled).map((section) => 
                        <Route exact path={ `/${ section.id }/` } key={ `/${ section.id }/` }>
                            <Section section={ section } />
                        </Route>
                    ) }

                    { data.sections.filter((section) => !section.disabled).map((section) => section.pages.map((page) => page.component && 
                        <Route exact path={ `/${ section.id }/${ page.id }` } key={ `/${ section.id }/${ page.id }` }>
                            <Page page={ page } section={ section } />
                        </Route>
                    )) }
                    <Redirect to="/" />
                </Switch>
            </Suspense>
        </div>
        <ToTop />
    </main>
}
