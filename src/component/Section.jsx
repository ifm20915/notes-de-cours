import React from 'react'
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';
import Bubble from './Bubble'

import styles from './Section.module.css'

export default function Section(props) {
    return <>
        <Helmet>
            <title>{ props.section.title }</title>     
            <meta name="description" content={ props.section.description } />
        </Helmet>

        <div className={ styles.header }>
            <Bubble name={ props.section.name } number={ props.section.number } />
            <h1>{ props.section.title }</h1>
        </div>

        { Object.values(props.section.groups).map((group, i) => (
            <section key={ i } className={ styles.group }>
                <h2>{ group.label }</h2>
                <ul>
                    { group.pages.map((page, i) => (
                        <li key={ i }>
                            { page.component ?
                                <Link to={ page.id }>{ page.title }</Link> :
                                <a href={ page.url } target="_blank" rel="noopener noreferrer">{ page.title }</a>
                            }
                        </li>
                    )) }
                </ul>
            </section>
        )) }
    </>
}
