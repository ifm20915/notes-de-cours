import React from 'react'

import styles from './Bubble.module.css'

export default function Bubble(props) {
    return  <div className={ styles.bubble }>
        <span className={ styles.name }>{ props.name }</span>
        <span className={ styles.number }>{ props.number }</span>
    </div>
}
