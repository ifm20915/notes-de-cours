import React, { useRef, useState } from 'react';
import useOnClickOutside from './useOnClickOutside'
import useCheckOverflow from './useCheckOverflow';
import AsideSection from './AsideSection'
import data from '../data';

import styles from './Aside.module.css';

export default function Aside(props) {
    const [open, setOpen] = useState(false);
    const asideRef = useRef(null);
    const buttonRef = useRef(null);
    const overflow = useCheckOverflow();

    const toggleAside = () => {   
        if(window.innerWidth < 1200){
            document.body.classList.toggle('locked');
            setOpen((open) => !open);
        }
    };

    const closeAside = () => {
        if(open){
            toggleAside();
        }
    };

    useOnClickOutside([asideRef, buttonRef], closeAside);

    return <>
        <aside ref={ asideRef } className={ (open ? styles.open : '') + ' ' + (overflow ? styles.overflow : '') }>
            <nav>
                <ul>
                    { data.sections.map((section, indexSection) => (
                        <li key={ indexSection }>
                            <AsideSection 
                                { ...section } 
                                closeAside={ closeAside } />
                        </li>
                    )) }
                </ul>
            </nav>
        </aside>
        <button ref={ buttonRef } onClick={ toggleAside }>
            <i className={ 'material-icons ' + styles.icon }>keyboard_arrow_right</i>
        </button>
    </>
}
