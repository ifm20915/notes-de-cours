import React from 'react';
import { Link } from "react-router-dom";
import { Helmet } from 'react-helmet-async';
import Bubble from './Bubble'
import { formatUnsignedInt } from '../Utils';
import { naming } from '../settings';
import data from '../data';

import styles from './Home.module.css';

export default function Home() {
    return <>
        <Helmet>
            <title>{ naming.title }</title>
            <meta name="description" content={ `Page d'accueil du site du cours ${ naming.title }.` } />
        </Helmet>

        <ul className={ styles.list } >
            { data.sections.map((section, index) => (
                <li key={ index } className={ styles.info + ' ' + (section.disabled ? styles.disabled : '') }>
                    <Bubble name={ section.name } number={ formatUnsignedInt(index, 2) } />
                    <div className={ styles.description }>
                        { !section.disabled ? 
                            <Link to={ `/${ section.id }/`}><h2>{ section.title }</h2></Link>
                            :
                            <h2>{ section.title }</h2>
                        }
                        <p>{ section.description }</p>
                    </div>
                </li>
            )) }
        </ul>
    </>
}
