import React from 'react';
import { Link } from "react-router-dom";

import styles from './Breadcrumb.module.css';

export default function Breadcrumb(props) {
    return  <nav className={ styles.breadcrumb + ' ' + (props.section ? '' : styles.always) }>
        { props.section &&
            <span>
                <Link to={ `/${ props.section.id }/` }>
                    { props.section.completeName }
                </Link>
                <span className={ styles.separator }>&gt;</span>
            </span>
        }
        <span>
            { props.section ? 
                ( props.page.group.label )
            :
                <Link to={ `/group/${ props.page.group.id }` }>
                    { props.page.group.label }
                </Link>
            }
            <span className={ styles.separator }>&gt;</span>
        </span>
        <span>
            { props.page.title }
        </span>
    </nav>
}
