import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import styles from './PageSwitcher.module.css';

export default function PageSwitcher(props) {
    let pageList;
    let pathStart;
    if(props.section){
        pageList = props.section.pages;
        pathStart = `/${ props.section.id }/`;
    }
    else{
        pageList = props.page.group.pages;
        pathStart = `/group/${ props.page.group.id }/`;
    }

    const getIndexFromSection = () => {
        for(let i = 0 ; i < pageList.length ; i++){
            if(pageList[i] === props.page){
                return i;
            }
        }
    }

    const [index, setIndex] = useState(getIndexFromSection());

    const getNextIndex = () => {
        for(let i = index + 1 ; i < pageList.length ; i++){
            if(!pageList[i].component){
                continue;
            }

            return i;
        }

        return null;
    }

    const getPreviousIndex = () => {
        for(let i = index - 1 ; i >= 0 ; i--){
            if(!pageList[i].component){
                continue;
            }

            return i;
        }

        return null;
    }
    
    let nextIndex = getNextIndex();
    let previousIndex = getPreviousIndex();

    const incrementIndex = () => {
        previousIndex = index;
        setIndex(nextIndex);
        nextIndex = getNextIndex();
    }

    const decrementIndex = () => {
        nextIndex = index;
        setIndex(previousIndex);
        previousIndex = getPreviousIndex();
    }

    return <div className={ styles.switcher + ' ' + (previousIndex === null ? styles.first : (nextIndex === null ? styles.last : '')) } >
        { previousIndex !== null &&
            <Link to={ `${ pathStart }${ pageList[previousIndex].id }` } className={ styles.previous } onClick={ decrementIndex }>
                <span>&lt;&lt;</span> <span>Page précédente</span>
            </Link>
        }
        { nextIndex !== null &&
            <Link to={ `${ pathStart }${ pageList[nextIndex].id }` } className={ styles.next } onClick={ incrementIndex }>
                <span>Page suivante</span> <span>&gt;&gt;</span>
            </Link>
        }
    </div>
}