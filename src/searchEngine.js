import data from './data';

class SearchEngine {
    constructor(){
        this.tags = {};

        for(let section of data.sections){
            for(let page of section.pages){
                // Add page title as a tag
                this.addPageToTag(page.title.toLowerCase(), page, section.id);

                // Add page group as a tag
                this.addPageToTag(page.group.label.toLowerCase(), page, section.id);

                // Add all tags
                for(let tag of page.tags){
                    this.addPageToTag(tag.toLowerCase(), page, section.id);
                }
            }
        }
    }

    addPageToTag(tag, page, sectionId){
        if(!this.tags[tag]){
            this.tags[tag] = [];
        }

        this.tags[tag].push({ page: page, sectionId: sectionId });
    }

    search(text){
        text = text.toLowerCase();
        let results = new Map();
        for(let tag of Object.keys(this.tags)){
            if(tag.includes(text)){
                for(let pageInfo of this.tags[tag]){
                    if(!results.has(pageInfo.page.id)){
                        results.set(pageInfo.page.id, pageInfo);
                    }
                }
            }
        }

        return Array.from(results.values());
    }
}

export default SearchEngine;
