import React from 'react';
import CodeBlock from '../component/CodeBlock';
import IC from '../component/InlineCode';

const baseHtml = 
`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Document</title>
</head>
<body>
    
</body>
</html>`;

export default function Revision() {
    return <>
        <section>
            <h2>Structure de base</h2>
            <p>
                Le HTML définit la structure de base de notre page Web. Il
                ne doit pas être utilisé pour définir le visuel de la page 
                Web. Le HTML de base d'une page Web ressemble à ceci:
            </p>
            <CodeBlock language="html">
                { baseHtml }
            </CodeBlock>
            <p>
                La balise <IC>{ '<head>' }</IC> contient les métadonnées 
                de la page, comme son titre, son type d'encodage ou encore 
                son icône. Cette balise contient aussi tous les fichiers à
                charger avant que le reste de la page ne soit traité par 
                le navigateur, comme les fichers de style CSS ou certains
                fichier Javascript.
            </p>
            <p>
                La balise <IC>{ '<body>' }</IC> contient le corps de la 
                page Web. Bref, tout ce qui est affiché à l'écran se 
                retrouve dans cette balise. La majorité de votre code HTML
                se retrouvera à cet endroit.
            </p>
        </section>

        <section>
            <h2>Balises HTML</h2>
            <p>
                Comme mentionné ci-dessus, la plupart des autres balises 
                HTML que vous utiliserez se retrouveront dans la 
                balise <IC>{ '<body>' }</IC>. Ces balises pourront servir
                entre autre à créer des divisions, des liste, des lien ou 
                encore des formulaires. 
                
                Pour plus d'information sur les 
                différentes balises, je vous pouvez vous rendre au lien 
                suivant:
            </p>
            <p>
                Je ne ferai pas de révision de chaque balise HTML. Si vous avez besoin de plus d'information sur 
                l'utilisation de certaines balises, il existe déjà de nombreuses ressources en ligne à ce sujet. Le 
                lien suivant est une bonne ressource pour vous remettre en mémoire les balises HTML disponibles:
            </p>
            <p>
                <a href="https://www.w3schools.com/tags/default.asp" target="_blank" rel="noopener noreferrer">HTML Element Reference</a>
            </p>
            <p>
                Apprendre à utiliser les ressources disponibles en ligne vous aidera grandement dans vos études et 
                pour votre travail dans le futur.
            </p>
        </section>

        <section>
            <h2>Bonnes pratiques</h2>
            <p>
                Voici une liste des bonnes pratiques que vous devez suivre lorsque vous écrivez du HTML:
            </p>
            <ul>
                <li>
                    <p>
                        Indenter correctement votre code avec des tabulations ou des espaces. Un code bien indenté est 
                        plus facile à lire et contient généralement moins d'erreurs. Si vous avez de la difficulté à 
                        garder l'indentation de votre code, n'hésitez pas à utiliser l'indentation automatique de votre 
                        éditeur de code (<IC>SHIFT + ALT + F</IC> dans Visual Studio Code sur Windows).
                    </p>
                </li>
                <li>
                    <p>
                        Valider le HTML pour s'assurer qu'il est standard. Un HTML valide sera interpréter de la même 
                        façon par tous les navigateurs, sera plus facile à maintenir et sera mieux classé par les moteurs 
                        de recherche comme Google, Bing ou Yahoo. Pour valider votre HTML, vous pouvez utiliser l'outil 
                        suivant:
                    </p>
                    <p>
                        <a href="https://validator.w3.org/" target="_blank" rel="noopener noreferrer">Markup Validation Service</a>
                    </p>
                </li>
                <li>
                    <p>
                        Utiliser les balises sématiques <IC>{'<header>'}</IC>, <IC>{'<main>'}</IC>, <IC>{'<footer>'}</IC>, <IC>{'<nav>'}</IC> et <IC>{'<section>'}</IC>.
                        Ces balises simplifient énormément la lecture du HTML et donnent de bonne indications aux 
                        moteurs de recherche comme Google, Bing ou Yahoo.
                    </p>
                </li>
                <li>
                    <p>
                        Spécifier un titre convenable pour vos pages. La balise <IC>{'<title>'}</IC> doit toujours 
                        contenir un titre décrivant bien la page de votre site web.
                    </p>
                </li>
                <li>
                    <p>
                        Suivre l'ordre des titres et sous-titres dans votre page. Les balises <IC>{'<h1>'}</IC> à <IC>{'<h6>'}</IC> doivent
                        toujours apparaître dans l'ordre dans une page web. Bref, le premier titre à apparaître dans 
                        une page web doit toujours être le <IC>{'<h1>'}</IC> et vous ne devez jamais passer par dessus 
                        un titre. Par exemple, passer d'un <IC>{'<h1>'}</IC> à un <IC>{'<h3>'}</IC> sans avoir de 
                        balise <IC>{'<h2>'}</IC> entre les deux n'est pas valide.
                    </p>
                </li>
                <li>
                    <p>
                        Rendre votre site web le plus accessible possible. Dans le HTML, c'est surtout de s'assurer 
                        que l'information sur votre page est disponible pour les lecteurs de page. De cette façon, une 
                        personne ayant des problèmes de vue peut accéder à toute l'information sur votre page web. 
                        Voici quelques cas à régler dans vos pages web:
                    </p>
                    <ul>
                        <li>Mettre une description pour chaque image dans leur attribut <IC>alt</IC>.</li>
                        <li>Pour chaque champ d'entrer de formulaire, mettre un <IC>{'<label>'}</IC> qui est lié au bon champ.</li>
                        <li>Dans chaque balise <IC>{'<section>'}</IC>, la première balise doit être un titre <IC>{'<h1>'}</IC> à <IC>{'<h6>'}</IC></li>
                    </ul>
                </li>
            </ul>
        </section>
    </>;
}
