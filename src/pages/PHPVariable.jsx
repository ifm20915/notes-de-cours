import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const varName = 
`<?php 
    $nom = "Wilkie";
    $prenom = "Jonathan";

    // Affiche Bonjour Jonathan Wilkie
    echo "Bonjour " . $prenom . " " . $nom;

    $nom = 42;

    // Affiche Bonjour Jonathan 42
    echo "Bonjour " . $prenom . " " . $nom
?>`;

const varGlobal = 
`<?php 
    // Variable globale
    $nombre = 1234;
?>`;

const varGlobalFunction =
`<?php 
    $nombre = 0;

    function incremente(){
        // Utiliser le mot-clé "global" pour accéder
        // à la variable globale "$nombre"
        global $nombre;
        $nombre++;
    }

    incremente();
    echo $nombre;
?>`;

const varLocal = 
`<?php 
    function afficheBonjour(){
        // Variable locale
        $nom = "Jonathan";
        echo "Bonjour " . $nom;
    }

    afficheBonjour();

    // Lance une erreur puisque la variable n'existe 
    // pas ici
    echo "Bonjour " . $nom;
?>`;

const stringConcat = 
`<?php 
    $uneChaine = "Allo";
    $uneChaine = $uneChaine . " toi";
    echo $uneChaine;
?>`;

const int = 
`<?php 
    $unNombreEntier = 42;
    $unNombreEntier = ($unNombreEntier - 13) % 10 * 6 + 15;
    echo $unNombreEntier . "<div>Nice</div>";
?>`;

const float =
`<?php 
    $unNombreFlottant = 3.14159;
    $unNombreFlottant *= 2;
    echo $unNombreFlottant;
?>`;

const boolean = 
`<?php 
    $unBooleen = (2 + 3 === 5);
    $unBooleen = !$unBooleen;
    echo $unBooleen ? 'true' : 'false';
?>`;

const array =
`<?php 
    $unTableau = array("Une chaine", 42, true, "Autre chaine");
    var_dump($unTableau);
?>`;

const arrayCount = 
`<?php 
    $unTableau = array(0, 1, 2, 3, 4,5, 6, 7, 8, 9);
    echo count($unTableau);
?>`;

const varNull = 
`<?php 
    $rien = null;
    var_dump($rien);
?>`;

export default class PHPVariable extends React.Component {
    render() {
        return <>
            <section>
                <h2>Variables</h2>
                <p>
                    Pour créer une variable en PHP, nous devont toujours la précéder du symbole <IC>$</IC>. Comme dans 
                    la plupart des langages de programmation, le nom des varaibles peut contenir des lettre majuscule 
                    ou minuscule, des chiffres et des barres de soulignement. On utilisera généralement le CamelCase
                    pour déclarer des variables.
                </p>
                <p>
                    Les variables en PHP sont faiblement typées, comme dans le langage Javascript. Cela veut dire que 
                    nous n'avons pas besoin de spécifier le type de la variable lors de sa création. Même s'il n'est 
                    pas recommandé de le faire, vous pouvez donc changer le type d'une variable juste en changeant sa 
                    valeur.
                </p>
                <CodeBlock language="php">{ varName }</CodeBlock>
            </section>

            <section>
                <h2>Portée</h2>
                <p>
                    Les variables en PHP ont 3 possibilités de portée:
                </p>
                <ul>
                    <li>Globale</li>
                    <li>Statique</li>
                    <li>Locale</li>
                </ul>
                <p>
                    
                </p>
                <dl>
                    <dt>Variable globale</dt>
                    <dd>
                        Variable qui est déclaré au premier niveau, directement entre les balises <IC>&lt;?php ... ?&gt;</IC> de 
                        PHP. Ces variables sont disponible pour le fichier entier et tous les autres fichiers inclus 
                        par dans ce fichier PHP.
                        <CodeBlock language="php">{ varGlobal }</CodeBlock>
                        Une variable globale est facile à utiliser, mais n'est pas directement accessible à 
                        l'intérieur d'une fonction. Si vous voulez y accéder dans une fonction, vous devrez utiliser 
                        le mot-clé <IC>global</IC> de vant le nom de la variable.
                        <CodeBlock language="php">{ varGlobalFunction }</CodeBlock>
                    </dd>

                    <dt>Variable locale</dt>
                    <dd>
                        Une variable locale est une variable déclaré à l'intérieur d'une fonction. Ces variables 
                        existent uniquement à l'intérieur de la fonction. Dès que l'exécution sort de la fonction, les 
                        variables locales sont automatiquement supprimées.
                        <CodeBlock language="php">{ varLocal }</CodeBlock>
                    </dd>

                    <dt>Variable statique</dt>
                    <dd>
                        Nous n'utiliserons pratiquement jamais les variables statiques. Les variables globales et 
                        locales seront toutefois souvent utilisé. Pour plus d'information, vous pouvez aller voir le 
                        manuel de PHP:
                        <p>
                            <a href="https://www.php.net/manual/en/language.oop5.static.php" target="_blank" rel="noopener noreferrer">
                                Static properties
                            </a>
                        </p>
                    </dd>
                </dl>
            </section>

            <section>
                <h2>Types de données</h2>
                <p>
                    Le langage PHP supporte 7 types différents de données. Essentiellement, vous pouvez mettre une 
                    valeur de un de ces types dans une variable sans aucun problème. Voici les 7 types de données et 
                    une brève introduction à comment les utiliser.
                </p>
                <dl>
                    <dt>String</dt>
                    <dd>
                        C'est la chaîne de caractère. Pour créer une String en PHP, on utilise les guillemets 
                        doubles <IC>""</IC>. Les variables de type String s'utilisent pratiquement de la même façon en 
                        PHP que dans les autres lanagage de programmation à l'exception d'une chose: la concaténation 
                        se fait avec l'opérateur <IC>.</IC>.
                        <CodeBlock language="php">{ stringConcat }</CodeBlock>
                        PHP offre aussi plusieurs fonctions de manipulation des String. Pour plus de détails, vous 
                        pouvez aller voir le lien suivant:
                        <p>
                            <a href="https://www.w3schools.com/php/php_ref_string.asp" target="_blank" rel="noopener noreferrer">
                                PHP String Functions
                            </a>
                        </p>
                    </dd>
                </dl>
                <dl>
                    <dt>Integer</dt>
                    <dd>
                        C'est le nombre entier. Il s'utilise de la même façon que dans les autres langages de 
                        programmation. Vous pouvez utiliser les mêmes opérateurs arithmétiques, de comparaison, 
                        d'incrémentation et de décrémentation que les autres langages de programmation auxquels vous 
                        êtes habitué.
                        <CodeBlock language="php">{ int }</CodeBlock>
                    </dd>
                </dl>
                <dl>
                    <dt>Float</dt>
                    <dd>
                        C'est le nombre flottant, aussi nommé nombre à virgule. Il s'utilise de la même façon que la 
                        variable Integer et de la même façon que dans les autres langages de programmation.
                        <CodeBlock language="php">{ float }</CodeBlock>
                    </dd>
                </dl>
                <dl>
                    <dt>Boolean</dt>
                    <dd>
                        C'est la variable booléenne. Elle peut contenir uniquement les 
                        valeurs <IC>true</IC> ou <IC>false</IC>. Vous pouvez utiliser les opérateurs booléens sur ces 
                        variables de la même façon que les autres langages de programmation auxquels vous êtes 
                        habitué. Vous pouvez aussi utiliser ces variables dans les structures de contrôle, comme dans 
                        les conditions et les boucles.
                        <CodeBlock language="php">{ boolean }</CodeBlock>
                    </dd>
                </dl>
                <dl>
                    <dt>Array</dt>
                    <dd>
                        C'est le tableau de données. Elle peut contenir un ensemble de données. Bien que l'on 
                        l'appelle un tableau, c'est plutôt une liste puisqu'on peut ajouter et retirer des données de 
                        celui-ci, de façon très similaire au Javascript. Pour créer un tableau en PHP, nous 
                        utiliserons la fonction <IC>array()</IC>:
                        <CodeBlock language="php">{ array }</CodeBlock>
                        PHP offre aussi plusieurs fonctions de manipulation des tableaux. Certaines pourront peut-être 
                        vous surprendre. Par exemple, pour avoir la taille d'un tableau, nous devons utiliser la 
                        fonction <IC>count()</IC> sur celui-ci. Pour plus de détails, vous pouvez aller voir le lien 
                        suivant:
                        <p>
                            <a href="https://www.w3schools.com/php/php_ref_array.asp" target="_blank" rel="noopener noreferrer">
                                PHP Array Functions
                            </a>
                        </p>
                        <CodeBlock language="php">{ arrayCount }</CodeBlock>
                    </dd>
                </dl>
                <dl>
                    <dt>Object</dt>
                    <dd>
                        C'est les variables contenant des instances de classes, ensembles de variables avec 
                        fonctionnalités. Les objets en PHP fonctionnent de façon similaire à Java ou C# puisqu'il 
                        nécessite la création de classes pour exister. Si vous voulez plus d'information sur les 
                        objets, vous pouvez vous rendre aux liens suivants:
                        <p>
                            <a href="https://www.php.net/manual/en/language.oop5.php" target="_blank" rel="noopener noreferrer">
                                Classes and Objects
                            </a>
                        </p>
                        <p>
                            <a href="https://www.php.net/manual/en/language.types.object.php" target="_blank" rel="noopener noreferrer">
                                Objects
                            </a>
                        </p>
                    </dd>
                </dl>
                <dl>
                    <dt>Null</dt>
                    <dd>
                        Indique simplement que la variable ne contient rien. Quand une variable est créé sans valeur, 
                        le langage PHP lui donne automatiquement la valeur <IC>null</IC>.
                        <CodeBlock language="php">{ varNull }</CodeBlock>
                    </dd>
                </dl>
            </section>
        </>;
    }
};
