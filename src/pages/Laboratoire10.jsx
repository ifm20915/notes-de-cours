import React from 'react';
import DownloadBlock from '../component/DownloadBlock'

import distibue from '../resources/laboratoire-10-distribué.zip'
import solution from '../resources/laboratoire-10-solution.zip'

class Laboratoire10 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    À partir de la solution du laboratoire 8, transformer l'application TODO pour qu'elle affiche son 
                    information en temps réel. Vous devrez utiliser les server sent event pour:
                </p>
                <ul>
                    <li>Envoyer les TODO ajoutés à tous les clients</li>
                    <li>Indiquer un changement d'état de checkbox à tous les clients</li>
                </ul>
            </section>

            <section>
                <h2>Fonctionnalités supplémentaires</h2>
                <p>
                    Si vous voulez un peu plus de pratique, n'hésitez pas à ajouter les fonctionnalités 
                    supplémentaires suivantes à votre projet:
                </p>
                <ul>
                    <li>
                        Sauvegarder les TODO dans un fichier JSON pour pouvoir les recharger lorsque le serveur 
                        redémarre.
                    </li>
                    <li>
                        Ajouter un moyen de supprimer les TODO et que la suppression se fasse en temps réel sur tous 
                        les clients.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Solution</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ distibue } name="distribué.zip"></DownloadBlock.File>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}

export default Laboratoire10;
