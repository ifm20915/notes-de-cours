import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'
import ColoredBox from '../component/ColoredBox'

const formSubmit = 
`let form = document.getElementById('form-login');

form.addEventListener('submit', (event) => {
    event.preventDefault();

    // ...
});`;

const valdationProperty = 
`let password = document.getElementById('password');

console.log(password.validity.valid);
console.log(password.validity.valueMissing);

if(password.validity.tooShort){
    // ...
}`;

const validationFunction = 
`// Input à valider
let password = document.getElementById('password');

// Champs d'erreur pour afficher les messages
//d'erreurs
let passwordErrorField = document.getElementById('password-error-field');

// Fonction de validation
const validatePassword = () => {
    if(password.validity.valid){
        // S'il n'y a pas d'erreur, on affiche
        // aucun message
        passwordErrorField.innerText = '';
    }
    else{
        // S'il y a une erreur, on affiche un
        //  message dépendant de l'erreur
        if(password.validity.valueMissing){
            passwordErrorField.innerText = 'Veuillez entrer votre mot de passe.';
        }
        else if(password.validity.tooShort){
            passwordErrorField.innerText = 'Votre mot de passe est trop court.';
        }
    }
};`;

const htmlErrorField = 
`<input type="password" id="password">
<div id="password-error-field" class="error-field">`;

const cssError = 
`input:invalid.active {
    border: 2px solid #800;
}

.error-field {
    display: none
    background-color: #800;
    color: #fff;
}

.error-field.active {
    display: block;
}`;

const validationFunctionStyle = 
`const validatePassword = () => {
    if(password.validity.valid){
        passwordErrorField.innerText = '';

        // Retirer les classe CSS s'il n'y a pas d'erreur
        passwordErrorField.classList.remove('active');
        password.classList.remove('active');
    }
    else{
        if(password.validity.valueMissing){
            passwordErrorField.innerText = 'Veuillez entrer votre mot de passe.';
        }
        else if(password.validity.tooShort){
            passwordErrorField.innerText = 'Votre mot de passe est trop court.';
        }

        // Ajouter les classe CSS s'il y a des erreurs
        passwordErrorField.classList.add('active');
        password.classList.add('active');
    }
};`;

const executeValidation = 
`password.addEventListener('input', validatePassword);
password.addEventListener('blur', validatePassword);
form.addEventListener('submit', validatePassword);`;

const checkValidity =
`form.addEventListener('submit', (event) => {
    event.preventDefault();

    if(form.checkValidity()){
        // Envoyer le formulaire à l'aide d'un POST (fetch)
    }
});`;

const customValidation = 
`const validateConfirm = () => {
    // Validation personnalisée pour la
    // confirmation du mot de passe
    if(password.value !== confirm.value){
        // Utilisation de "setCustomValidity" pour
        // indiquer une erreur personnalisée
        confirm.setCustomValidity('invalide-confirm');
    }
    else {
        confirm.setCustomValidity('');
    }

    // Code pour changer l'affichage s'il y a
    // des erreurs
    if(confirm.validity.valid){
        confirmErrorField.innerText = '';
        confirmErrorField.classList.remove('active');
        confirm.classList.remove('active');
    }
    else{
        // Utilisation de l'attribut "customError"
        // pour voir s'il y a une erreur 
        // personnalisée qui ne valide pas
        if(confirm.validity.customError){
            confirmErrorField.innerText = 
                'Votre mot de passe et sa confirmation ne sont pas pareil.';
        }

        confirmErrorField.classList.add('active');
        confirm.classList.add('active');
    }
}

confirm.addEventListener('input', customConfirm);
confirm.addEventListener('blur', customConfirm);
form.addEventListener('submit', customConfirm);`;

export default function ValidationClient() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Lorsque vous travaillez sur une application qui envoit des données écrites ou spécifiées par un 
                utilisateur, il est très important de les valider. La validation du côté client a quelques 
                avantages:
            </p>
            <ul>
                <li>
                    Indiquer rapidement à l'utilisateur s'il y a des erreurs dans les valeurs qu'il a entré
                </li>
                <li>
                    Empêcher d'envoyer des données erronées au serveur
                </li>
                <li>
                    Diminuer la quantité d'appel au serveur
                </li>
            </ul>
            <p>
                Dans cette page, nous verrons comment valider le client à l'aide des contraintes de validation.
                La validation se fait généralement dans le code Javascript du client, mais il est possible de 
                simplifier un peu le code à l'aide de certains attributs dans le HTML.
            </p>
            <p>
                Puisque la validation peut être longue, je vous suggère de l'ajouter dans son propre fichier 
                Javascript sur le client.
            </p>
        </section>

        <section>
            <h2>Validation dans le HTML</h2>
            <p>
                Il est possible, directement dans le HTML, d'indiquer le type de validation à faire sur 
                un <IC>{ '<input>' }</IC>,  un <IC>{ '<textarea>' }</IC> ou un <IC>{ '<select>' }</IC> à l'aide de 
                certains attributs. Les voici:
            </p>
            <dl>
                <dt>required</dt>
                <dd>
                    Spécifie que le champ doit nécessairement être rempli. On l'utilise de la façon suivante:
                    <CodeBlock language="html">{ '<input type="text" required>' }</CodeBlock>
                </dd>
                <dt>minlength et maxlength</dt>
                <dd>
                    Spécifie un minimum ou un maximum de caractères pour les chaînes de caractères. On l'utilise 
                    de la façon suivante:
                    <CodeBlock language="html">{ '<input type="text" minlength="10" maxlength="100">' }</CodeBlock>
                </dd>
                <dt>min et max</dt>
                <dd>
                    Spécifie un minimum ou un maximum pour les données numériques (ex: <IC>{ '<input type="number">' }</IC>). 
                    On l'utilise de la façon suivante:
                    <CodeBlock language="html">{ '<input type="range" min="0" max="100">' }</CodeBlock>
                </dd>
                <dt>type</dt>
                <dd>
                    Spécifie le type de données que nous voulons recevoir. Bien que l'on utilise principalement 
                    cette propriété pour définir le visuel d'un <IC>{ '<input>' }</IC>, il est parfois aussi 
                    utilisé pour la validation. Le type <IC>email</IC> et <IC>url</IC> valident que le texte entré
                    est bien sous une forme valide. On l'utilise de la façon suivante:
                    <CodeBlock language="html">{ '<input type="email">' }</CodeBlock>
                </dd>
                <dt>pattern</dt>
                <dd>
                    Spécifie un patron (regex) que doit respecter la chaîne de caractères entrée. Cet attribut est 
                    très pratique pour s'assurer que certains champs de textes respectent un certain format 
                    (ex: code postal ou téléphone). Vous n'avez pas besoin de connaître les regex. Vous pouvez 
                    simplement les trouver sur le Web. On l'utilise de la façon suivante:
                    <CodeBlock language="html">{ '<input type="text" pattern="^[A-Za-z]\\d[A-Za-z][ -]?\\d[A-Za-z]\\d$">' }</CodeBlock>
                </dd>
            </dl>
            <ColoredBox heading="À noter">
                Si vous voulez faire d'autres types de validation ou des validations plus complexe, vous devrez 
                les programmer en Javascript vous-même.
            </ColoredBox>
        </section>

        <section>
            <h2>Comportements par défaut dans un <IC>form</IC></h2>
            <p>
                Si vous ajoutez des <IC>{ '<input>' }</IC>, <IC>{ '<textarea>' }</IC> ou <IC>{ '<select>' }</IC> dans 
                un <IC>{ '<form>' }</IC> et que vous utilisez les attributs de validation vu ci-dessus, votre 
                navigateur fera une validation par défaut de ces champs. Cette validation par défaut est 
                intéressante, mais possède quelques lacunes:
            </p>
            <ul>
                <li>On ne peut pas changer le message d'erreur</li>
                <li>La langue du message d'erreur n'est pas nécessairement celui de la page Web</li>
            </ul>
            <p>
                Nous allons donc généralement désactiver la valiation par défaut pour avoir une validation plus 
                personnalisée. Pour ce faire, vous n'avez qu'à ajouter l'attribut <IC>novalidate</IC> à votre 
                formulaire:
            </p>
            <CodeBlock language="html">{ '<form novalidate> ... </form>' }</CodeBlock>
            <p>
                De plus, nous voulons désactiver le comportement par défaut lors de la soumission d'un formulaire.
                Par défaut, un formulaire est automatiquement envoyé lorsqu'un utilisateur clique sur un bouton de 
                type submit dans le formulaire. Ce comportement sert principalement pour les serveurs qui 
                utilisent encore le type de contenu HTTP <IC>multipart/form-data</IC>. Dans notre cas, nous 
                voulons tout envoyer en JSON (<IC>application/json</IC>), donc nous désactiverons ce comportement 
                dans le javascript.
            </p>
            <CodeBlock language="js">{ formSubmit }</CodeBlock>
        </section>

        <section>
            <h2>Propriété de validation</h2>
            <p>
                Pour chaque champ qui a un attribut de validation, nous pourrons vérifier dans le code Javascript si 
                le champ en question respecte sa validation à l'aide des propriétés suivantes:
            </p>
            <dl>
                <dt>validity.valid</dt>
                <dd>
                    Indique si le champ est valide selon les attributs HTML que l'on lui a ajouté
                </dd>
                <dt>validity.valueMissing</dt>
                <dd>
                    Si un champ a l'attribut <IC>required</IC>, cette variable indique si le champ est vide.
                </dd>
                <dt>validity.typeMismatch</dt>
                <dd>
                    Si un champ a un type <IC>email</IC> ou <IC>url</IC>, cette variable indique si le champ 
                    contient bien une adresse courriel ou un URL valide.
                </dd>
                <dt>validity.tooShort</dt>
                <dd>
                    Si un champ a l'attribut <IC>minlength</IC>, cette variable indique si le champ est trop court.
                </dd>
                <dt>validity.toolong</dt>
                <dd>
                    Si un champ a l'attribut <IC>maxlength</IC>, cette variable indique si le champ est trop long.
                </dd>
                <dt>validity.rangeUnderflow</dt>
                <dd>
                    Si un champ a l'attribut <IC>min</IC>, cette variable indique si la valeur numérique du champ 
                    est trop petite.
                </dd>
                <dt>validity.rangeOverflow</dt>
                <dd>
                    Si un champ a l'attribut <IC>max</IC>, cette variable indique si la valeur numérique du champ 
                    est trop grande.
                </dd>
                <dt>validity.patternMismatch</dt>
                <dd>
                    Si un champ a l'attribut <IC>pattern</IC>, cette variable indique si la valeur du champ ne 
                    respecte pas le patron spécifié.
                </dd>
            </dl>
            <p>
                Vous pouvez les utiliser de la façon suivante en Javascript:
            </p>
            <CodeBlock language="js">{ valdationProperty }</CodeBlock>
        </section>

        <section>
            <h2>Fonction de validation</h2>
            <p>
                Puisque nous avons désactiver la validation par défaut, nous devrons faire une bonne partie de la 
                validation à la main. Vous devrez donc créer une fonction de validation distincte pour chaque 
                champ que vous voulez valider. Dans ces fonctions, nous pourrons utiliser les propriétés 
                booléennes vu ci-dessus pour nous aider dans notre validation.
            </p>
            <p>
                Une fonction de validation ressemble généralement à ceci:
            </p>
            <CodeBlock language="js">{ validationFunction }</CodeBlock>
            <p>
                Vous remarquerez l'utilisation d'un champ d'erreur <IC>passwordErrorField</IC>. Ce champ est 
                généralement simplement un <IC>{ '<div>' }</IC> ou un <IC>{ '<span>' }</IC> qui contiendra votre 
                message d'erreur. Je vous suggère fortement d'en ajouter un pour chaque champ que vous voulez 
                valider.
            </p>
            <CodeBlock language="html">{ htmlErrorField }</CodeBlock>
        </section>

        <section>
            <h2>Styler les champs en erreur</h2>
            <p>
                Si vous voulez bien styler les champs lorsqu'ils ont une erreur, vous pouvez utiliser les règles 
                CSS suivantes:
            </p>
            <CodeBlock language="css">{ cssError }</CodeBlock>
            <p>
                Vous pouvez, bien entendu, modifier les styles pour qu'ils soient approprié pour votre page.
            </p>
            <p>
                Vous remarquerez aussi l'utilisation d'une classe <IC>active</IC>. Nous ajouterons ou retirerons 
                généralement cette classe à la main dans nos fonctions de validation de la façon suivante:
            </p>
            <CodeBlock language="js">{ validationFunctionStyle }</CodeBlock>
        </section>

        <section>
            <h2>Lancer les fonctions de validation</h2>
            <p>
                Nous devons maintenant lancer nos fonctions de validation. Chaque fonction de validation devra 
                généralement être lancé à 3 endroits différend:
            </p>
            <ul>
                <li>Quand on change la valeur du champ</li>
                <li>Quand on dé-focus le champ (lorsqu'on clique à l'extérieur du champ)</li>
                <li>Quand on essaie d'envoyer le formulaire</li>
            </ul>
            <CodeBlock language="js">{ executeValidation }</CodeBlock>
            <p>
                Une fois les fonctions de validation exécuté sur tous ces évènements, nous pourrons maintenant 
                valider si le formulaire est valide avant de l'envoyer. Pour ce faire, nous utiliserons la 
                fonction <IC>checkValidity()</IC> du formulaire.
            </p>
            <CodeBlock language="js">{ checkValidity }</CodeBlock>
        </section>

        <section>
            <h2>Validation personnalisée</h2>
            <p>
                Il est possible de créer des validations personnalisées qui font plus que les validations de base.
                Dans ces cas, nous devrons nous-même programmer le code de la validation puisqu'il n'existera pas
                d'attribut HTML pour le faire pour nous. Pour ce faire, nous pourrons utiliser la
                fonction <IC>setCustomValidity()</IC> pour indiquer une erreur et <IC>validity.customError</IC> pour voir si un champ contient une erreur de 
                validation personnalisée.
            </p>
            <p>
                Prenons par exemple le cas du mot de passe qui demande une confirmation dans un 2<sup>ème</sup> champ.
                Le fait que le champ de confirmation doit avoir la même valeur que le champ de mot de passe 
                constitut une validation personnalisé. Nous pourrions écrire le code suivant pour le valider:
            </p>
            <CodeBlock language="js">{ customValidation }</CodeBlock>
            <p>
                Comme dans le code ci-dessus, assurez-vous que le code de la validation personnalisé ci-dessus est
                exécuté avant celui qui change l'interface graphique pour que l'erreur s'affiche correctement à
                l'écran.
            </p>
        </section>
    </>;
}
