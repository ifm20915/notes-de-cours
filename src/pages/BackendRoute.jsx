import React from 'react';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox'
import IC from '../component/InlineCode';

const expressRoute = 
`/** Création du serveur */
let app = express();

// Ajout de middlewares
// ...

// Ajout des routes
app.get('/liste-to-do', (request, response) => {
    // Code à exécuter quand on reçoit un GET 
    // sur la route /liste-to-do
});

app.post('/liste-to-do', (request, response) => {
    // Code à exécuter quand on reçoit un POST 
    // sur la route /liste-to-do
});

app.delete('/liste-to-do', (request, response) => {
    // Code à exécuter quand on reçoit un DELETE 
    // sur la route /liste-to-do
});

app.get('/truc/machin/chose', (request, response) => {
    // Code à exécuter quand on reçoit un GET 
    // sur la route /truc/machin/chose
});`;

const expressRouteShort = 
`app.route('/liste-to-do')
    .get((request, response) => {
        // ...
    })
    .post((request, response) => {
        // ...
    })
    .delete((request, response) => {
        // ...
    });`;

const expressRouter = 
`// Fichier "routes.js"
import { Router } from 'express';

const router = Router();

router.get('/liste-to-do', (request, response) => {
    // ...
});

router.post('/liste-to-do', (request, response) => {
    // ...
});

export default router;`;

const expressRouterUse = 
`// Fichier "server.js"
import express from 'express';
import routerExterne from './routes.js';

/** Création du serveur */
let app = express();

// Ajout de middlewares
// ...

// Charger les routes du fichier externe dans notre application
app.use(routerExterne);`;


const request =
`app.post('/liste-to-do', function(request, response){
    console.log(request.body);

    // ...

});`;

const requestRouteParam = 
`app.get('/salles/:nomSalle', function(request, response){
    console.log(request.params.nomSalle);

    // ...

});`;

const requestQueryParam = 
`app.get('/salles', function(request, response){
    // Va chercher les données dans les paramètres de l'URL
    // ex: http://localhost/salles?nomSalle="blah"
    console.log(request.query.nomSalle);

    // ...

});`;

const response = 
`app.get('/liste-to-do', function(request, response){
    // Renvoyer des données de divers formats
    response.send('Texte à envoyer');

    // Renvoyer des données au format JSON
    response.json({ key: value });

    // Renvoyer des code de status avec leur titre
    response.sendStatus(200); // OK
});`;

const responseStatus = 
`app.get('/liste-to-do', function(request, response){
    // Changer le code de status avant d'envoyer une réponse
    response.status('400').send('Texte à envoyer');
    response.status('200').json({ key: value });
});`;

const responseEmpty = 
`app.delete('/liste-to-do', function(request, response){
    // Réponse vide
    response.status('200').end();
});`;

const notFound = 
`app.use(function (request, response) {
    // Renvoyer simplement une chaîne de caractère
    // indiquant que la page n'existe pas
    response.status(404).send(request.originalUrl + ' not found.');
});`;

export default function BackendRoute() {
    return <>
        <section>
            <h2>Ajouter des routes</h2>
            <p>
                Une fois les middlewares ajoutés, il ne nous reste qu'à ajouter les actions de notre serveur. Pour 
                ce faire, nous devons indiquer à Express quoi faire si on reçoit des requêtes HTTP ayant certaines 
                méthodes à une adresse, ou route, spécifié. Je vous suggère fortement de planifier votre API 
                serveur avant cette étape pour facilité sa programmation.
            </p>
            <p>
                Voici comment ajouter du code sur des routes:
            </p>
            <CodeBlock language="js">{ expressRoute }</CodeBlock>
            <p>
                Si vous avez plusieurs méthodes sur la même route, comme dans l'exemple ci-dessus, il est possible 
                de les déclarer ensemble pour raccourcir légèrement le code comme ceci:
            </p>
            <CodeBlock language="js">{ expressRouteShort }</CodeBlock>
            <ColoredBox heading="Attention">
                À moins d'indication contraire, vous devez ajouter vos routes après l'ajout des middlewares dans 
                votre application. L'ordre des middlewares et des routes et très important dans Express puisque c'est l'ordre 
                dans lequel ils seront traité pour chaque requête reçu.
            </ColoredBox>
            <p>
                Vous constaterez que toutes les fonctions à exécuter par le serveur possèdent les 
                paramètres <IC>request</IC> et <IC>response</IC>. Le paramètre <IC>request</IC> nous permet 
                d'accéder aux informations de la requête HTTP envoyé par le client. Le 
                paramètre <IC>response</IC> nous permettra d'envoyer une réponse approprié aux requêtes reçu du client.
                Vous aurez plus d'information sur ces paramètres ci-dessous.
            </p>
        </section>

        <section>
            <h2>Ajouter des routes dans un fichier externe</h2>
            <p>
                Il est possible de déclarer les routes dans un autre fichier pour bien séparer le code. C'est une 
                pratique commune dans les gros projets pour faciliter la lecture du code.
            </p>
            <p>
                Le fichier de route externe pourrait ressembler à ceci:
            </p>
            <CodeBlock language="js">{ expressRouter }</CodeBlock>
            <p>
                Ce fichier programme un objet routeur contenant les différentes routes programmées dans ce fichier. 
                Ce routeur pourra par la suite être intégré dans Express dans le fichier principal de la façon 
                suivante:
            </p>
            <CodeBlock language="js">{ expressRouterUse }</CodeBlock>
        </section>

        <section>
            <h2>Request</h2>
            <p>
                La paramètre <IC>request</IC> nous servira généralement à accéder aux données envoyées par le client, mais aussi aux paramètres 
                de la requête. Si nous voulons accéder aux données de la requêtes dans un format lisible, nous devons toutefois utiliser le 
                middleware <IC>json</IC>. Ce middleware nous permettra d'analyser le contenu de la requête et de le rendre accessible 
                dans le paramètre <IC>request</IC>.
            </p>
            <p>
                Il existe de nombreux formats pour les données dans les requêtes, mais dans le cours, nous utiliserons majoritairement 
                le <IC>JSON</IC> puisque c'est le format le plus facile à utiliser en Javascript. N'oubliez pas d'ajouter ce middleware avec 
                la ligne suivante:
            </p>
            <CodeBlock language="js">app.use(json());</CodeBlock>
            <p>
                Par la suite, dans les fonctions exécutées par les routes, nous pourrons accéder aux données en
                utilisant l'objet <IC>body</IC> dans le paramètre <IC>request</IC> de la façon suivante:
            </p>
            <CodeBlock language="js">{ request }</CodeBlock>
            <p>
                Si les données ne sont pas dans le corps de la requête, mais plutôt dans la route, nous utiliserons 
                une syntaxe un peu différentes dans sa définition. Vous noterez d'ailleurs les deux-points (<IC>:</IC>) 
                devant une partie de la route, indiquant que cette partie de la route est une donnée envoyé par le 
                client. Nous irons aussi chercher les données différement, soit dans l'objet <IC>params</IC> dans le 
                paramètre <IC>request</IC>. Voici un exemple:
            </p>
            <CodeBlock language="js">{ requestRouteParam }</CodeBlock>
            <p>
                Si les données ont plutôt été passé dans les paramètres (query) de la requête, nous irons chercher les
                données de façon un peu différente, mais encore similaire. Dans ce cas-ci, les données seront dans 
                l'objet <IC>query</IC> dans le paramètre <IC>request</IC>. Voici un exemple:
            </p>
            <CodeBlock language="js">{ requestQueryParam }</CodeBlock>
        </section>

        <section>
            <h2>Response</h2>
            <p>
                Le paramètre <IC>response</IC> nous permettra d'envoyer une réponse au client à l'aide de plusieurs fonctions. Chaque fonction de 
                traitement de route doit retourner une réponse, même si celle-ci est vide. Voici une liste des fonctions que vous pouvez 
                utiliser pour retourner une réponse:
            </p>
            <CodeBlock language="js">{ response }</CodeBlock>
            <p>
                Si vous voulez changer le code de status de la réponse, vous pouvez utiliser la fonction <IC>status()</IC>. Je vous recommande 
                fortement de toujours indiquer le code de status pour qu'il soit clair et visible dans votre code.
            </p>
            <CodeBlock language="js">{ responseStatus }</CodeBlock>
            <p>
                Dans certains cas, il sera inutile de renvoyer des données avec la réponse. Par exemple, si vous faites un ajout à une base 
                de données et que la requête réussi sans problèmes, seul le code de status 200 est vraiment important dans la réponse. Pour ne
                rien retourner, nous utilisons la fonction <IC>end()</IC>:
            </p>
            <CodeBlock language="js">{ responseEmpty }</CodeBlock>
        </section>

        <section>
            <h2>Gérer les routes non définies</h2>
            <p>
                Si un client essai d'envoyer une requête à une route non défini, nous voulons généralement lui retourner un code de status 404 
                (Not found). Par défaut, si aucune route n'est trouvé, Express va renvoyer une erreur 404 avec du code HTML. Nous pouvons 
                changer le retour d'une erreur 404 en ajoutant notre propre middleware à la fin de notre programme de la façon suivante:
            </p>
            <CodeBlock language="js">{ notFound }</CodeBlock>
            <ColoredBox heading="Attention">
                Il est important de mettre ce middleware après la gestion des routes. Sinon, toutes vos routes
                retourneront une erreur 404 (Not found) au lieu de leur bonne valeur.
            </ColoredBox>
        </section>
    </>;
}