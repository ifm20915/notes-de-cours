import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const installMySQL = 
`npm install promise-mysql`;

const connexion =
`import mysql from 'promise-mysql';

const connectionPromise = mysql.createPool({
    connectionLimit: process.env.DB_CONNECTION_LIMIT,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
});

export default connectionPromise`;

const env =
`# Serveur
PORT=5000
NODE_ENV=development

# Base de données
DB_CONNECTION_LIMIT=10
DB_HOST=localhost
DB_USER=root
DB_PASSWORD=
DB_NAME=northwind`;

const productsGet = 
`import connectionPromise from './connection.js';

export const getProducts = async () => {
    // Attendre que la connexion à la base de données
    // soit établie
    let connection = await connectionPromise;

    // Envoyer une requête à la base de données
    let results = await connection.query(
        'SELECT * FROM products'
    );

    // Retourner les résultats
    return results;
}`;

const productsAdd =
`exports const addProduct = async (productCode, productName, 
    description, standardCost, listPrice) => 
{
    // Attendre que la connexion à la base de données
    // soit établie
    let connection = await connectionPromise;

    // Envoyer une requête à la base de données
    connection.query(
        \`INSERT INTO products(
            product_code, 
            product_name, 
            description, 
            standard_cost, 
            list_price)
         VALUES(?, ?, ?, ?, ?)\`,
        [productCode, productName, description, standardCost, listPrice]
    );
}`;

const ordersAdd =
`exports const addOrder = async (employeeId, customerId, productArray) => 
{
    // Attendre que la connexion à la base de données
    // soit établie
    let connection = await connectionPromise;

    // Créer le 'orders'
    let result = await connection.query(
        \`INSERT INTO orders(employee_id, customer_id)
        VALUES(?, ?)\`,
        [employeeId, customerId]
    );

    // Aller chercher le ID généré du 'orders'
    const orderId = result.insertId;

    // Créer les 'order_details'
    for(let product in productArray)
    {
        connection.query(
            \`INSERT INTO order_details(order_id, product_id, quantity)
             VALUES(?, ?, ?)\`,
            [orderId, product.id, product.quantity]
        );
    }
}`;

export default function DBMySQL() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                MySQL est un gestionnaire de base de données gratuit développé par Oracle. Vous avez déjà appris à
                utiliser MySQL, mais vous ne l'avez probablement jamais encore utilisé dans une application. Ici,
                c'est ce que nous ferons. Nous connecterons nos serveurs web à une base de données pour pouvoir
                gérer de grandes quantités de données tout en gardant de bonnes performances.
            </p>
            <p>
                Pour ouvrir une connexion à une base de données MySQL à partir de Node.js, nous aurons besoin de 
                télécharger une librairie de code supplémentaire:
            </p>
            <CodeBlock language="shell">{ installMySQL }</CodeBlock>
        </section>

        <section>
            <h2>Connexion</h2>
            <p>
                Une fois la librairie téléchargé, créez-vous un fichier qui va vous permettre d'établir une 
                connexion avec la base de données. Je nomme généralement ce fichier <IC>connection.js</IC>.
            </p>
            <CodeBlock language="js">{ connexion }</CodeBlock>
            <p>
                Lors de la connexion à la base de données, nous devons spécifier l'adresse de la base de données, 
                le nom d'utilisateur et le mot de passe pour se connecter et le nom de la base de données que vous 
                voulez utiliser. Nous spécifierons ces valeurs dans le fichier <IC>.env</IC> de la façon suivante:
            </p>
            <CodeBlock language="properties">{ env }</CodeBlock>
            <p>
                Si vous utilisez XAMPP, le nom d'utilisateur par défaut est <IC>root</IC> et le mot de passe est
                vide. En développement, le nombre de connexions peut rester à 10. En production, vous pourriez
                l'augmenter dépendant des performances de votre serveur. Finalement, le nom de la base de données
                doit être le nom d'une des bases de données existante sur votre gestionnaire de base de données
                MySQL.
            </p>
        </section>

        <section>
            <h2>Envoyer une requête SQL</h2>
            <p>
                Pour faire des requêtes SQL, je vous recommande de faire un fichier Javascript pour chaque table 
                que vous utiliserez dans votre base de données ou fonctionnalité de votre serveur. À titre
                d'exemple, j'utiliserai la table <IC>products</IC> de Northwind. J'ai donc créé un
                fichier <IC>products.js</IC> dans lequel j'écrirai mes requêtes SQL. Une autre option pourrait
                être de créer un fichier <IC>model.js</IC> dans un dossier <IC>/product</IC>. Vous auriez ainsi
                un fichier <IC>/product/model.js</IC>.
            </p>
            <CodeBlock language="js">{ productsGet }</CodeBlock>
            <p>
                Comme vous pouvez le constater, nous devrons utiliser notre fichier de connexion pour pouvoir 
                faire des requêtes à la base de données. Cette séparation du code dans plusierus fichiers est 
                vraiment nécessaire si vous utilisez plusieurs tables dans votre base de données.
            </p>
            <p>
                Vous remarquerez aussi que nous utilisons beaucoup de promesses. En effet, l'objet de connexion 
                est encapsulé dans une promesse. Nous devons donc utiliser un <IC>await</IC> pour avoir sa valeur. 
                De la même façon, la fonction <IC>query</IC> de la connexion retourne elle aussi une promesse. 
                Nous devons donc mettre un await pour avoir sa réponse. N'oubliez pas de mettre votre fonction 
                en <IC>async</IC> pour que les <IC>await</IC> ne lance pas d'erreurs.
            </p>
        </section>

        <section>
            <h2>Prévenir les injections de SQL</h2>
            <p>
                Si vous voulez plutôt envoyer des données à MySQL, comme lorsque vous faite une recherche 
                avec condition paramétrée ou lorsque vous faite une insertion de données, vous devez faire 
                attention à la façon d'insérer les données dans vos requêtes.
            </p>
            <p>
                En effet, si vous ajoutez directement les données dans la chaîne de caractères SQL, vous vous 
                ouvrez à un type de piratage qui se nomme l'injection de SQL. Pour prévenir ce problème, utilisez 
                la méthode suivante pour ajouter des données dans votre requête SQL.
            </p>
            <CodeBlock language="js">{ productsAdd }</CodeBlock>
            <p>
                Vous remarquerez que partout où nous voulons mettre des valeurs dans la requête SQL, nous mettrons 
                plutôt un point d'intérogation <IC>?</IC>. Nous fournirons par la suite un tableau contenant nos 
                données à la fonction <IC>query</IC> pour qu'elle puisse remplacer les <IC>?</IC> par nos valeurs 
                automatiquement et sécuritairement. L'ordre des valeurs dans le tableau doit être le même que 
                celui de l'apparition des <IC>?</IC> dans la requête SQL.
            </p>
        </section>

        <section>
            <h2>Identifiant de la dernière insertion</h2>
            <p>
                Lorsque vos modèles de base de données contiennent des identifiants autogénérés, il est très fréquent
                de vouloir insérer une rangée et ensuite insérer l'identifiant (ID) dans une autre table. Le problème,
                puisque l'identifiant est autogénéré, nous ne connaissons pas sa valeur. Par exemple, si vous voulez
                créer une commande dans la base de données Northwind, vous devez insérer des rangées dans la
                table <IC>orders</IC> et <IC>order_details</IC>. Toutefois, pour insérer les <IC>order_details</IC>,
                vous devrez préalablement avoir créer le <IC>orders</IC> pour y mettre son identifiant. Il est donc
                nécessaire de connaître l'identifiant du <IC>orders</IC> que l'on vient de créer
            </p>
            <p>
                Pour aller chercher l'identifiant de la dernière insertion, nous utiliserons le résultat retourné par
                MySQL. Celui-ci contient entre autre l'identifiant de l'élément que nous venons d'insérer. Nous
                l'utiliserons avec Node.js de la façon suivante:
            </p>
            <CodeBlock language="js">{ ordersAdd }</CodeBlock>
        </section>
    </>;
};
