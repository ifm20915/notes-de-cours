import React from 'react';
import IC from '../component/InlineCode'
import DownloadBlock from '../component/DownloadBlock'

import distibue from '../resources/laboratoire-ramen-distribué.zip'
import solution from '../resources/laboratoire-ramen-solution.zip'

export default function LaboratoireRamen() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                À partir du projet du projet distribué, compléter l'application de la commande de ramen pour 
                qu'elle fonctionne correctement. La base de l'application est déjà presque complète. Le client 
                peut déjà soumettre ses commandes au serveur à l'aide de son interface graphique. Sur le serveur, 
                les commandes sont stockées automatiquement dans le fichier <IC>orders.json</IC>.
            </p>
            <p>
                Il ne vous reste qu'à programmer la validation sur le client et le serveur.
            </p>
        </section>

        <section>
            <h2>Validation sur le serveur</h2>
            <p>
                Sur le serveur, on utilise la fonction <IC>isOrderValid</IC> dans le 
                fichier <IC>/orders/ordersValidation.js</IC> pour valider la commande envoyé par le client. Cette
                fonction retourne <IC>true</IC> ou <IC>false</IC> dépendant de si la commande envoyé est valide ou
                non.
            </p>
            <p>
                Pour tester cette validation, vous pouvez envoyer des commandes à partir de l'interface web.
                Si vous regardez dans l'onglet <IC>Network</IC> des outils de développement de votre navigateur,
                vous verrez les requêtes passer. Le retour 200 ou 400 vous indiquera le résultat de votre
                validation.
            </p>
            <p>
                Programmer cette fonction pour qu'elle fasse les validations suivantes:
            </p>
            <ul>
                <li>
                    Intensité
                    <ul>
                        <li>Doit avoir une des valeurs suivantes: <IC>weak</IC>, <IC>average</IC> et <IC>strong</IC></li>
                    </ul>
                </li>
                <li>
                    Richesse
                    <ul>
                        <li>Doit avoir une des valeurs suivantes: <IC>none</IC>, <IC>light</IC>, <IC>average</IC>, <IC>heavy</IC> et <IC>ultra</IC></li>
                    </ul>
                </li>
                <li>
                    Texture
                    <ul>
                        <li>Doit avoir une des valeurs suivantes: <IC>extra firm</IC>, <IC>firm</IC>, <IC>average</IC>, <IC>soft</IC> et <IC>extra soft</IC></li>
                    </ul>
                </li>
                <li>
                    Garlic
                    <ul>
                        <li>Doit avoir une des valeurs suivantes: <IC>none</IC>, <IC>little</IC>, <IC>average</IC>, <IC>lot</IC> et <IC>tooMuch</IC></li>
                    </ul>
                </li>
                <li>
                    Oignon vert
                    <ul>
                        <li>Doit avoir une des valeurs suivantes: <IC>none</IC>, <IC>white</IC> et <IC>whole</IC></li>
                    </ul>
                </li>
                <li>
                    Viande
                    <ul>
                        <li>Doit être un tableau</li>
                        <li>Doit contenir au maximum 3 éléments</li>
                        <li>Peut uniquement contenir les valeurs suivantes: <IC>pork</IC>, <IC>beef</IC> et <IC>chicken</IC></li>
                    </ul>
                </li>
                <li>
                    Garnitures
                    <ul>
                        <li>Doit être un tableau</li>
                        <li>Doit contenir au maximum 5 éléments</li>
                        <li>Peut uniquement contenir les valeurs suivantes: <IC>egg</IC>, <IC>bamboo</IC>, <IC>soja</IC>, <IC>algae</IC> et <IC>corn</IC></li>
                    </ul>
                </li>
                <li>
                    Prénom
                    <ul>
                        <li>Ne doit pas être nul ou vide</li>
                        <li>Doit être une chaîne de caractère</li>
                    </ul>
                </li>
                <li>
                    Nom
                    <ul>
                        <li>Ne doit pas être nul ou vide</li>
                        <li>Doit être une chaîne de caractère</li>
                    </ul>
                </li>
                <li>
                    Courriel
                    <ul>
                        <li>Doit être une chaîne de caractère</li>
                        <li>Doit être une adresse courriel valide</li>
                        <li>Au moins l'adresse courriel ou le numéro de téléphone doit être non nul ou vide.</li>
                    </ul>
                </li>
                <li>
                    Téléphone
                    <ul>
                        <li>Doit être une chaîne de caractère</li>
                        <li>Doit être un numéro de téléphone valide</li>
                        <li>Au moins l'adresse courriel ou le numéro de téléphone doit être non nul ou vide.</li>
                    </ul>
                </li>
                <li>
                    Adresse
                    <ul>
                        <li>Ne doit pas être nul ou vide</li>
                        <li>Doit être une chaîne de caractère</li>
                    </ul>
                </li>
            </ul>
        </section>

        <section>
            <h2>Validation sur le client</h2>
            <p>
                Sur le client, donc dans le fichier <IC>/public/js/ordersValidation.js</IC> et <IC>/public/index.html</IC>,
                ajouter les validations ci-dessous. Assurez-vous de désactiver la validation par défaut dans le
                formulaire et de mettre les bons attributs HTML aux bonnes places.
            </p>
            <p>
                Il existe déjà des classes CSS pour styler la validation client. Comme montré dans les notes de
                cours, vous pouvez ajouter et retirer la classe <IC>active</IC> sur les champs d'erreur et les
                inputs pour les styler automatiquement.
            </p>
            <ul>
                <li>
                    Input du prénom
                    <ul>
                        <li>Ne doit pas être vide</li>
                        <li>Au maximum 50 caractères</li>
                    </ul>
                </li>
                <li>
                    Input du nom
                    <ul>
                        <li>Ne doit pas être vide</li>
                        <li>Au maximum 50 caractères</li>
                    </ul>
                </li>
                <li>
                    Input du courriel
                    <ul>
                        <li>Doit être une adresse courriel valide</li>
                        
                    </ul>
                </li>
                <li>
                    Input du téléphone
                    <ul>
                        <li>Doit être un numéro de téléphone valide</li>
                    </ul>
                </li>
                <li>
                    Input de l'adresse
                    <ul>
                        <li>Ne doit pas être vide</li>
                        <li>Au maximum 200 caractères</li>
                    </ul>
                </li>
            </ul>
        </section>

        <section>
            <h2>Bonus</h2>
            <p>
                Si vous avez le temps, essayez d'ajouter une validation personnalisée qui valide que l'utilisateur
                fournit au moins une adresse courriel, un numéro de téléphone ou les 2. Bref, si un seul des 2 ou si
                les 2 sont entré, la validation passe. Si aucune valeur n'est mise dans aucun des 2, la validation
                échoue.
            </p>
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distibue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
