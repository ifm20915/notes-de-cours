import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

import utilisateur from '../resources/utilisateur.png';

const db =
    `import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

const connectionPromise = open({
    filename: process.env.DB_FILE,
    driver: sqlite3.Database
})
.then((connection) => {
    connection.exec(
        \`CREATE TABLE IF NOT EXISTS utilisateur(
            id_utilisateur INTEGER PRIMARY KEY,
            courriel TEXT NOT NULL UNIQUE,
            mot_de_passe TEXT NOT NULL,
            niveau_acces INTEGER
        );\`
    );

    return connection;
});

export default connectionPromise;`;

const addUser =
    `export async function addUtilisateur(courriel, motDePasse) {
    let connection = await connectionPromise;

    let motDePasseEncrypte = await bcrypt.hash(motDePasse, 10);

    await connection.run(
        \`INSERT INTO utilisateur(courriel, mot_de_passe, niveau_acces)
        VALUES (?, ?, 1)\`,
        [courriel, motDePasseEncrypte]
    )
}`;

const getUser =
    `export async function getUtilisateur(courriel) {
    let connection = await connectionPromise;

    const result = await connection.get(
        \`SELECT id_utilisateur, courriel, mot_de_passe, niveau_acces 
        FROM utilisateur
        WHERE courriel = ?\`,
        [courriel]
    );

    return result;
}`;

export default function AuthDatabase() {
    return <>
        <section>
            <h2>Modèle de données</h2>
            <p>
                Pour avoir une authentification simple par mot de passe, nous avons 2 données à sauvegarder pour nos
                utilisateur.
            </p>
            <ol>
                <li>
                    Un identifiant
                    <p>
                        Ce sera souvent un nom d'utilisateur unique ou encore une adresse courriel.
                    </p>
                </li>
                <li>
                    Un mot de passe
                    <p>
                        Nous sauvegarderons uniquement sa version encryptée.
                    </p>
                </li>
            </ol>
            <p>
                Il est aussi fréquent de voir d'autres champs, comme la date de dernière connexion, son niveau d'accès
                et autres informations personnelles utiles pour votre application web.
            </p>
        </section>

        <section>
            <h2>Table de données</h2>
            <p>
                Dans ce cours, nous utiliserons l'authentification avec une base de données SQL relationnelle. Une
                table contenant les données mentionnées ci-dessus pourrait ressembler à ceci:
            </p>
            <img src={utilisateur} alt="Table de données d'une classe utilisateur" />
            <p>
                Voici quelques petits détails à noter:
            </p>
            <ul>
                <li>
                    Il est possible de mettre la clé primaire sur le courriel unique et ainsi supprimer le
                    champ <IC>id_utilisateur</IC>
                </li>
                <li>
                    Il est possible de changer le champ du courriel par un champ du genre <IC>nom_utilisateur</IC>.
                </li>
                <li>
                    Le mot de passe chiffré prendra toujours 60 caractères, donc si votre base de données le
                    permet, spécifiez-le pour optimiser la mémoire.
                </li>
                <li>
                    Le niveau d'accès peut être relié à une autre table indiquant le nom de chaque niveau d'accès.
                    Ça peut être une bonne pratique pour augmenter la compréhension de nos données.
                </li>
            </ul>
            <p>
                Si vous utilisez SQLite, le code suivant dans un fichier <IC>connection.js</IC> pourrait créer une
                base de données avec notre table d'utilisateur:
            </p>
            <CodeBlock language="js">{db}</CodeBlock>
        </section>

        <section>
            <h2>Requête de données</h2>
            <p>
                Je vous suggère donc de vous créer un fichier <IC>/model/utilisateur.js</IC> dans lequel vous
                programmerez vos requêtes SQL sur votre serveur. Pour le bon fonctionnement de ce fichier, n'oubliez
                pas d'importer votre fichier de connection à la base de donnée. Vous devriez donc avoir une
                instruction similaire en haut de votre fichier:
            </p>
            <CodeBlock language="js">{`import connectionPromise from "../connection.js";`}</CodeBlock>
            <p>
                Dans le cas d'une authentification simple, nous aurons seulement 2 requêtes à programmer pour accéder
                à notre base de données
            </p>
            
            <h3>Requête pour créer un utilisateur</h3>
            <p>
                Cette requête nous permettra de créer un utilisateur, par exemple lorsqu'un compte est créé
                sur notre site web au travers d'une page d'inscription.
            </p>
            <CodeBlock language="js">{addUser}</CodeBlock>
            <p>
                Vous noterez ici l'utilisation de <IC>bcrypt</IC> pour encrypter le mot de passe dans la base
                de données. Assurez-vous donc d'importer la librairie de code dans votre fichier pour qu'il
                fonctionne correctement.
            </p>
            <CodeBlock language="js">{`import bcrypt from 'bcrypt';`}</CodeBlock>

            <h3>Requête pour chercher un utilisateur par son identifiant</h3>
            <p>
                Dans notre cas, l'identifiant sera son adresse courriel, mais ce pourrait être aussi son nom
                d'utilisateur. Nous utiliserons cette requête lorsque nous voulons valider le mot de passe
                d'un utilisateur et mettre ses données dans la session.
            </p>
            <CodeBlock language="js">{getUser}</CodeBlock>
        </section>
    </>
}