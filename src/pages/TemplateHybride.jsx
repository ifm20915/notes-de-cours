import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import gabarit from '../resources/gabarit-hybride.zip';

export default function TemplateHybride() {
    return <>
        <section>
            <h2>Guide d'utilisation</h2>
            <p>
                Pour utiliser ce gabarit, veuillez suivre les étapes suivates:
            </p>
            <ol>
                <li>Télécharger le gabarit</li>
                <li>Décompresser le gabarit</li>
                <li>Changer le nom du dossier du projet</li>
                <li>
                    Ouvrir le fichier <IC>package.json</IC>
                    <ol>
                        <li>Modifier le <IC>name</IC> du package</li>
                        <li>Modifier la <IC>description</IC> du package</li>
                        <li>Modifier l' <IC>author</IC> du package</li>
                    </ol>
                </li>
                <li>
                    Ouvrir un terminal dans le dossier du gabarit
                    <ol>
                        <li>Lancer la commande <IC>npm install</IC></li>
                        <li>Lancer la commande <IC>npm start</IC></li>
                    </ol>
                </li>
            </ol>
            <p>
                Ce gabarit contient un projet express vide avec le middleware de serveur statique ainsi que d'autres 
                middlewares et outils de développement pratique pour créer un serveur web hybride simple.
            </p>
        </section>

        <section>
            <h2>Fichiers supplémentaires</h2>
            <p>
                J'ai ajouté une structure de base de site web dans le dossier <IC>public</IC>. Si vous n'avez pas besoin de 
                tous ces fichiers, n'hésitez pas à les supprimer.
            </p>
        </section>

        <section>
            <h2>Téléchargement</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ gabarit } name="gabarit-hybride.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>
}