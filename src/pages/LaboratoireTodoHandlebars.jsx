import React from 'react';
import Video from '../component/Video'
import DownloadBlock from '../component/DownloadBlock'

import distribue from '../resources/laboratoire-todo-handlebars-distribue.zip'
import solution from '../resources/laboratoire-todo-handlebars-solution.zip'

export default function LaboratoireTodoHandlebars() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                À partir du gabarit fourni ci-dessous, modifiez le projet pour utiliser le rendu serveur avec Handlebars là où il est 
                recommandé de le faire.
            </p>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Laboratoire - Handlebars - Exemple Todo" src="https://www.youtube.com/embed/80LV9r6oxw0?si=fTOUMWGb7UG5bbJp" />
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribue.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
