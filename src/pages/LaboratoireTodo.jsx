import React from 'react';
import Video from '../component/Video'
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-todo-solution.zip'

export default function LaboratoireTodo() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                À partir du gabarit de projet hybride d'express, construire une application selon la mise 
                en situation et la vidéo suivante.
            </p>
        </section>

        <section>
            <h2>Mise en situation</h2>
            <p>
                Vous voulez faire une application de liste TODO sur laquelle votre liste sera sauvegardé sur un 
                serveur et où vous pourrez ajouter des éléments à votre liste et les cocher en cliquant sur une 
                boîte à cocher. L'interface de votre application doit être dans une page Web.
            </p>
        </section>

        <section>
            <h2>Vidéo d'exemple</h2>
            <Video title="Présentation - Laboratoire 8" src="https://www.youtube.com/embed/i2cdqYNFvKE" />
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
