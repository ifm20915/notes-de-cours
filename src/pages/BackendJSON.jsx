import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const xml = 
`<?xml version="1.0" encoding="UTF-8"?>
<joindre>
    <usager>Jonathan Wilkie<usager>
    <salle>Web serveur<salle>
</joindre>`;

const json = 
`{
    "usager": "Jonathan Wilkie",
    "salle": "Web serveur"
}`;

const array = 
`[ 
    25, 
    "Jonathan", 
    true 
]`;

const object = 
`{ 
    "id": 25, 
    "nom": "Jonathan", 
    "estBeau": true 
}`

export default function BackendJSON() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Bien que nous utilisions le protocole HTTP pour communiquer entre le client et le serveur, le format 
                dans lequel nous envoyons les données peut grandement changer. Voici quelques formats de données qui 
                sont utilisés:
            </p>
            <dl>
                <dt>Texte</dt>
                <dd>
                    Probablement la façon la plus simple d'envoyer des données. Simplement envoyer du texte. Le gros 
                    désavantage est que c'est tellement simple que dès qu'on essait d'envoyer des données plus 
                    complexe, comme des objets ou des tableaux, ça devient difficile à faire.
                </dd>
                <dt>urlencoded</dt>
                <dd>
                    C'est un format qui stocke les informations sous un format textuel, mais avec une syntaxe 
                    simpliste nous permettant d'envoyer plusieurs données facilement. Voici à quoi ça ressemble:
                    <div><IC>salle=Web+serveur&amp;utilisateur=Jonathan+Wilkie</IC></div>
                    Le urlencoded est parfois encore utilisé aujourd'hui pour les formulaires HTML qui l'utilisent par 
                    défaut, mais autrement, son manque de flexibilité, surtout face aux objets, tableaux et typage le 
                    rendent plutôt obsolète. Il est aussi difficile à lire...
                </dd>
                <dt>XML</dt>
                <dd>
                    C'est un format de données qui ressemble beaucoup au HTML. C'est essentiellement un fichier texte, 
                    mais qui respecte une certaine syntaxe pour qu'il soit facile d'y mettre des informations 
                    complexes qui peuvent être facilement traduite par le client et le serveur. Ça ressemble à ceci:
                    <CodeBlock language="xml">{ xml }</CodeBlock>
                    Le XML est moins utilisé aujourd'hui principalement par sa syntaxe ambigu, la lourdeur de sa 
                    syntaxe ainsi que le manque de typage dans les données.
                </dd>
                <dt>JSON</dt>
                <dd>
                    C'est un format de données qui est basé sur les objets Javascript. JSON est d'ailleurs l'acronyme 
                    pour JavaScript Object Notation. C'est un format textuel, comme le XML, mais avec une syntaxe plus 
                    légère, un support de base du typage des données et une lecture agréable. De plus, puisque nous 
                    programmons nos clients et nos serveurs en Javascript, ce type est facile à utiliser. Le JSON 
                    ressemble à ceci:
                    <CodeBlock language="json">{ json }</CodeBlock>
                    Le JSON est le format de données le plus utilisé aujourd'hui. Il est donc important de le 
                    comprendre. C'est le format que nous utiliserons dans le cours.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Types de données</h2>
            <p>
                Le JSON supporte 6 types de données. Les voici:
            </p>
            <ul>
                <li>
                    Les chaines de caractères (string). Les chaines de caractères doivent être entre 
                    guillemets <IC>"Jonathan"</IC>.
                </li>
                <li>
                    Les nombres (number). Les nombres peuvent être entier ou à virgule <IC>42</IC>.
                </li>
                <li>
                    Les booléens (boolean). Accepte uniquement les valeurs <IC>true</IC> ou <IC>false</IC>.
                </li>
                <li>
                    La valeur nulle (null). Indique une valeur inexistante <IC>null</IC>.
                </li>
                <li>
                    Les tableaux (array). C'est un ensemble de données variées qui n'ont pas besoin d'être du même 
                    type. Les tableaux doivent être entre accolades carrées.
                    <CodeBlock language="json">{ array }</CodeBlock>
                </li>
                <li>
                    Les objets (object). C'est un regroupement de variables de différents types. Les objets doivent 
                    être entre accolades, les différents noms de variable doivent être entre guillemets, le nom des 
                    variables et leur valeur doivent être séparé par un deux-points et finalement, chaque variable 
                    doit ¸etre séparé par une virgule <IC></IC>.
                    <CodeBlock language="json">{ object }</CodeBlock>
                </li>
            </ul>
            <ColoredBox heading="À noter">
                Dans le cas des tableaux et objets, il n'est pas nécessaire de formatter le JSON sur plusieurs lignes. 
                C'est simplement plus simple à lire si on le fait.
            </ColoredBox>
        </section>

        <section>
            <h2>JSON dans le protocole HTTP</h2>
            <p>
                Dans le protocole HTTP, pour des raisons de sécurité, il n'est pas possible d'envoyer uniquement des 
                chaînes de caractère, des nombres, des booléens ou la valeur nulle. Il faut absolument encapsuler ces 
                valeurs dans un objet.
            </p>
            <p>
                Par exemple, si on veut envoyer le nom de l'utilisateur à supprimer en JSON, on ne peut pas simplement
                envoyer la données suivante: 
            </p>
            <CodeBlock language="json">"Dudette1337"</CodeBlock>
            <p>
                Il faudra plutôt l'encapsuler dans un objet comme il suit:
            </p>
            <CodeBlock language="json">{ '{ "nom": "Dudette1337" }' }</CodeBlock>
            <p>
                Il est possible de configurer un serveur pour qu'il accepte les valeurs uniques, mais celà ne vaut pas 
                les problèmes de sécurité potentiels qui pourrait être occasionnés. N'hésitez donc vraiment pas à toujours 
                envoyer vos données dans des objets JSON. 
            </p>
        </section>
    </>
}
