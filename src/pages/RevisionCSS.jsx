import CodeBlock from "../component/CodeBlock";
import IC from "../component/InlineCode";

const baseCss = 
`selecteur {
    propriété1: valeur1;
    propriété2: valeur2;
    propriété3: valeur3;
}`;

const linkCss = 
`<head>
    ...
    <title>Document</title>

    <link rel="stylesheet" href="./css/style.css">
</head>`;

const wrapperCss = 
`.wrapper {
    max-width: 50rem;
    margin: 0 auto;
    padding: 0 1rem;
}`;

export default function RevisionCSS() {
    return <>
        <section>
            <h2>Fichier CSS</h2>
            <p>
                Le CSS défini le visuel de notre page Web. Un fichier CSS 
                est composé d'une multitude de règles qui serviront à 
                appliquer des styles en fonction de certaines condition.
                Ces conditions défini à l'aide de <strong>sélecteur
                </strong> et les styles sont défini par des paires 
                de <strong>propriétés</strong> et <strong>valeurs
                </strong>. Une règle CSS ressemble à ceci:
            </p>
            <CodeBlock language="css">
                { baseCss }
            </CodeBlock>
            <p>
                Il existe une multitude de façon d'utiliser les sélecteurs pour définir nos conditions de style. Comme 
                pour les balises HTML, je ne ferai pas de révision sur les sélecteurs CSS disponibles. Je vous recommande 
                fortement d'aller trouver des ressources en ligne à ce sujet, comme le site Web suivant pour avoir un 
                aperçu des différentes façon d'utiliser les sélecteurs:
            </p>
            <p>
                <a href="https://www.w3schools.com/cssref/css_selectors.asp" target="_blank" rel="noopener noreferrer">CSS Selector Reference</a>
            </p>
            <p>
                Pour ce qu'il est des propriétés CSS et leurs valeurs, ils y en a une quantité phénoménale et qui, 
                dans certains cas, ne fonctionnent que sous certaines conditions. N'hésitez pas à utiliser un moteur 
                de recherche si vous ne savez pas comment styler certaines choses. Je vous recommande tout de même le 
                site Web suivant pour avoir une idée de toutes les propriétés existantes:
            </p>
            <p>
                <a href="https://developer.mozilla.org/fr/docs/Web/CSS/Reference" target="_blank" rel="noopener noreferrer">Référence CSS</a>
            </p>
        </section>

        <section>
            <h2>Liaison avec le HTML</h2>
            <p>
                Pour lier un fichier CSS à un fichier HTML, il faut ajouter une balise <IC>{'<link>'}</IC> dans la 
                balise <IC>{'<head>'}</IC> du HTML. Voici un exemple:
            </p>
            <CodeBlock language="html">
                { linkCss }
            </CodeBlock>
            <p>
                Il est possible d'avoir des liasons sur plusieurs fichiers CSS. Pour ce faire, vous devez simplement 
                ajouter plusieurs balises <IC>{'<link>'}</IC> dans votre HTML. Dans ce genre de cas, les fichiers CSS 
                sont téléchargés et exécutés dans l'ordre. Il faut donc faire attention puisqu'il est possible que des 
                règles CSS d'un deuxième fichier écrasent celle d'un premier fichier.
            </p>
        </section>

        <section>
            <h2>Bonnes pratiques</h2>
            <p>
                Voici une liste des bonnes pratiques que vous devez suivre lorsque vous stylez une page web avec du 
                CSS:
            </p>
            <ul>
                <li>
                    <p>
                        Rendre votre site web plus accessible en s'assurant que les couleurs de fond et de texte ont un 
                        très bon contraste. De cette façon, les personnes ayant des troubles de vision comme le daltonisme 
                        pourront bien voir le contenu de votre site web. Vous pouvez utiliser une ressource comme celle-ci 
                        pour tester vos sites web:
                    </p>
                    <p>
                        <a href="https://www.toptal.com/designers/colorfilter/" target="_blank" rel="noopener noreferrer">Colorblind Web Page Filter</a>
                    </p>
                </li>
                <li>
                    <p>
                        Rendre votre site web disponible sur mobile. Si votre site web est réactif (responsive), il sera 
                        autant disponible sur grand que sur petit écran. Aujourd'hui, selon plusieurs sources, plus de 50% 
                        du traffic sur nos sites web provient d'un appareil mobile. Il est donc important que nos pages 
                        web puissent s'afficher correctement à tout types de clientèle.
                    </p>
                </li>
                <li>
                    <p>
                        Assurer que le design de votre site web est ergonomique. Bien que ce ne soit pas toujours votre 
                        travail, l'ergonomie, donc la facilité et l'efficacité d'utilisation de l'interface graphique est 
                        très importante. Une application ou un site web qui n'est pas ergonomique va généralement avoir 
                        beaucoup moins d'utilisateur. Si vous n'avez pas de designer graphique pour vous aider, vous 
                        pouvez tout de même essayer suivre certaines règles:
                    </p>
                    <ul>
                        <li>Les éléments d'intéraction (boutons, liens, etc.) doivent être assez gros et facilement cliquable.</li>
                        <li>La taille du texte doit être assez grande.</li>
                        <li>La majorité de l'information sur le site web doit être accessible en 3 clics.</li>
                    </ul>
                </li>
            </ul>
        </section>

        <section>
            <h2>Trucs et astuces</h2>
            <p>
                Le langage CSS contient beaucoup de propriétés différentes. Certaines choses sont faciles à faire, comme 
                changer la couleur du texte ou la couleur de fond. Toutefois, lorsqu'il est temps de faire la mise en page 
                et de positionner les éléments de votre page web, le travail peut s'avérer plus difficile. Voici quelques 
                trucs et astuces pour vous aider à y arriver:
            </p>
            <ul>
                <li>
                    <p>
                        Apprendre à utiliser le <IC>display: flex</IC>. La disposition en flex permet de mettre des 
                        éléments un à côté de l'autre ou un en dessous de l'autre. Vous pouvez même mettre des dispositions
                        flex dans d'autres disposition flex, ce qui vous permet de faire presque n'importe quel interface
                        graphique. Si vous avez toujours de la difficulté à utiliser les flex, je vous recommande le guide 
                        suuivant:
                    </p>
                    <p>
                        <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank" rel="noopener noreferrer">A Complete Guide to Flexbox</a>
                    </p>
                </li>
                <li>
                    <p>
                        Éviter d'utiliser certaines vieilles propriétés CSS pour votre mise en page qui compliquent votre 
                        tâche. Les propriétés <IC>float</IC> et <IC>clear</IC>, par exemple, ne devrait pas être utilisé pour 
                        positionner les éléments dans votre page. Apprenez plutôt à utiliser les flex. Ils sont plus simple et
                        plus efficace.
                    </p>
                </li>
                <li>
                    <p>
                        Utiliser les <IC>position</IC> uniquement lorsque c'est nécessaire. Il semble parfois intéressant 
                        d'utiliser les <IC>position</IC> avec les propriétés <IC>top</IC>, <IC>right</IC>, <IC>bottom</IC> et <IC>left</IC> pour 
                        faire la mise en page d'éléments, mais en général, ça ne fait que compliquer votre travail. Le 
                        positionnement devrait être utiliser uniquement lorsque vous voulez une superposition d'éléments ou 
                        lorsque des éléments doivent être positionnés de façon relative à leur position originale.
                    </p>
                </li>
                <li>
                    <p>
                        Connaître les techniques pour centrer des éléments. Il y a principalement 2 techniques à connaître:
                    </p>
                    <ul>
                        <li>Utiliser <IC>margin: 0 auto</IC> pour centrer horizontalement.</li>
                        <li>
                            Utiliser <IC>display: flex</IC> avec <IC>align-items: center</IC> pour centrer verticalement 
                            et <IC>justify-content: center</IC> pour centrer horizontalement.
                        </li>
                    </ul>
                </li>
                <li>
                    <p>
                        Connaître le principe du wrapper. Le principe est simplement de donner une largeur maximale à la 
                        page sur les grands écrans. Souvent, on va aussi centrer le contenu. Voici à quoi ressemble le 
                        code d'un wrapper:
                    </p>
                    <CodeBlock language="css">{ wrapperCss }</CodeBlock>
                    <p>
                        Dans votre page, je vous recommande de mettre des wrapper sur des <IC>{'<div>'}</IC> qui se 
                        retrouveront à l'intérieur des balises <IC>{'<header>'}</IC>, <IC>{'<main>'}</IC> et <IC>{'<footer>'}</IC>
                    </p>
                </li>
            </ul>
        </section>
    </>;
}