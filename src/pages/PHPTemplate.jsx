import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';
import WebExample from '../component/WebExample';

import phpTemplate from '../resources/php-template.png';

const phpHTML = 
`<?php 
    echo "
        <h1>Titre</h1>
        <p>
            Lorem, ipsum dolor sit amet consectetur 
            adipisicing elit. In dicta hic, officia 
            laborum <strong>architecto</strong> 
            praesentium non amet error.
        </p>
    ";
?>`;

const htmlFromPPHP = 
`<h1>Titre</h1>
<p>
    Lorem, ipsum dolor sit amet consectetur 
    adipisicing elit. In dicta hic, officia 
    laborum <strong>architecto</strong> 
    praesentium non amet error.
</p>`;

const phpInHTML = 
`<?php
    $titre = "Titre de la page";
    $contenu = "Lorem ipsum dolor, sit amet " .
        "consectetur adipisicing elit. Officiis " .
        "qui repudiandae nulla magnam unde quas " .
        "nisi ullam ducimus eius amet?";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $titre ?></title>
</head>
<body>
    <h1><?php echo $titre ?></h1>
    <p><?php echo $contenu ?></p>
</body>
</html>`;

const phpControl = 
`<?php
    $rainbowName = array(
        "rouge", "orange", "jaune", "vert", 
        "bleu", "indigo", "violet"
    );
    $rainbowCode = array(
        "#ff0000", "#ff7f00", "#ffff00", "#00ff00", 
        "#0000ff", "#4b0082", "#9400d3"
    );
?>

<div style="display: flex; align-items: center;">
    <div style="flex: 1;">
        <?php for($i = 0 ; $i < count($rainbowName) ; $i++) { ?>
            <div style="background-color: <?php echo $rainbowCode[$i]; ?>;">
                <?php echo $rainbowName[$i]; ?>
            </div>
        <?php } ?>
    </div>
    <div>
        <img src="http://www.nyan.cat/cats/original.gif" 
            alt="cat"
            style="transform: translateX(-5rem)">
    </div>
</div>`;

const phpHeader = 
`<!-- fichier header.php -->
<header>
    <h1><a href="/">Titre</a></h1>
    <nav>
        <ul>
            <li><a href="/propos">À propos</a></li>
            <li><a href="/contact">Contactez-nous</a></li>
        </ul>
    </nav>
</header>`;

const phpUseHeader = 
`<!-- fichier index.php -->
<?php include 'header.php'; ?>
<main>
    Contenu
</main>`;

export default class PHPTemplate extends React.Component {
    render() {
        return <>
            <section>
                <h2>Créer des fichiers HTML</h2>
                <p>
                    Jusqu'à présent, le langage PHP ressemble beaucoup à d'autres langages de programmation. Ça seule 
                    différence, c'est qu'au lieu d'afficher ses résultats dans une console, il les affiche, dans un 
                    navigateur Web. Cette méthode d'affichage bizarre est expliqué par le fait que PHP sert à créer 
                    des document à partir du serveur. Voici un schémas pour vous aider à comprendre:
                </p>
                <img src={ phpTemplate } alt="Exécution du PHP"/>
                <p>
                    Ce que nous voyons dans le navigateur est donc le résultat de ce que notre code PHP retourne. Si 
                    notre serveur PHP retourne du HTML, que ce passe-t-il alors?
                </p>
                <CodeBlock language="php">{ phpHTML }</CodeBlock>
                <WebExample>
                    <WebExample.Code type="html" display={ false }>{ htmlFromPPHP }</WebExample.Code>
                </WebExample>
                <p>
                    On ici que le HTML retourné sera interprété par le navigateur. Cela nous permet donc de générer 
                    dynamiquement des pages HTML sur le serveur Apache et de les retourner au client.
                </p>
            </section>

            <section>
                <h2>Insérer le PHP dans le HTML</h2>
                <p>
                    Comme vous pouvez le penser, si nous devons gérer le code HTML comme une chaîne de caractère à 
                    l'intérieur de notre fichier PHP, ce n'est vraiment pas pratique. PHP nous simplifie heureusement 
                    la tâche. Nous pouvons effectivement écrire directement du HTML dans le fichier PHP. Nous 
                    insèrerons plutôt le PHP à l'intérieur du HTML de la façon suivante:
                </p>
                <CodeBlock language="php-template">{ phpInHTML }</CodeBlock>
                <p>
                    Vous remarquerez dans le code ci-dessus les insertions de PHP à divers endroits dans le HTML. Ce 
                    code fonctionne sans problème et est beaucoup plus facile à lire et à comprendre que si le HTML 
                    était dans une chaîne de caractères.
                </p>
            </section>

            <section>
                <h2>Structure de contrôles avec le HTML</h2>
                <p>
                    La force du templating de PHP ne s'arrête pas là. En effet, il est possible d'utiliser des 
                    conditions et des boucles à l'intérieur du HTML pour générer des pages HTML complètement 
                    dynamique à partir de données en mémoire sur le serveur ou provenant d'une base de données.
                </p>
                <CodeBlock language="php-template">{ phpControl }</CodeBlock>
                <p>
                    Dans l'exemple ci-dessus, nous utilisons des données venant de tableaux dans le PHP pour générer 
                    un arc-en-ciel. Nous utilisons une boucle <IC>for</IC> pour répéter du code HTML pour chaque 
                    valeur dans les tableaux.
                </p>
                <p>
                    Avec ce genre de code, nous pouvons aussi rendre des affichages conditionnel. Avec des <IC>if</IC>, 
                    nous pouvons regarder une condition et afficher du HTML seulement si celui-ci est valide.
                </p>
            </section>

            <section>
                <h2>Séparation dans plusieurs fichiers</h2>
                <p>
                    Si vous désirer séparer la génération du HTML dans plusieurs fichiers, vous pouvez le faire grâce 
                    au mot-clé <IC>include</IC>. Le mot-clé <IC>include</IC> permet de spécifier un autre fichier PHP 
                    à aller exécuter dans votre code. Ce genre de code est très pratique pour les morceaux de code qui 
                    se répètent d'une page à l'autre, comme les en-tête, pied de page ou les menus de votre site Web.
                </p>
                <CodeBlock language="php-template">{ phpHeader }</CodeBlock>
                <CodeBlock language="php-template">{ phpUseHeader }</CodeBlock>
                <ColoredBox heading="À noter">
                    Un fichier PHP qui ne contient que du HTML, sans aucune insertion de code PHP, comme dans le 
                    fichier <IC>header.php</IC> ci-dessus, est tout à fait valide.
                </ColoredBox>
            </section>
        </>;
    }
};
