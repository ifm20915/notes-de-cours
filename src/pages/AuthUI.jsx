import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const form =
`<section>
    <h2>Connexion</h2>

    <form id="form-connexion" class="form-authentification">
        <label for="input-courriel">Courriel</label>
        <input type="email" id="input-courriel">

        <label for="input-mot-de-passe">Mot de passe</label>
        <input type="password" id="input-mot-de-passe">
        
        <input type="submit" value="Connexion">
    </form>
</section>`;

const fetching =
`let inputCourriel = document.getElementById('input-courriel');
let inputMotDePasse = document.getElementById('input-mot-de-passe');
let formConnexion = document.getElementById('form-connexion');

formConnexion.addEventListener('submit', async (event) => {
    event.preventDefault();

    // Les noms des variables doivent être les mêmes
    // que celles spécifié dans les configuration de
    // passport dans le fichier "authentification.js"
    const data = {
        courriel: inputCourriel.value,
        motDePasse: inputMotDePasse.value
    };

    let response = await fetch('/connexion', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    });

    if (response.ok) {
        // Si l'authentification est réussi, on
        // redirige vers une autre page
        window.location.replace('/');
    }
    else if(response.status === 401) {
        // Si l'authentification ne réussi pas, on
        // a le message d'erreur dans l'objet "data"
        let data = await response.json()
        
        // Utiliser "data" pour afficher l'erreur ;a
        // l'utilisateur ici ...
    }
});`;

const fetchingInsc =
`if(response.ok) {
    // Si la création de compte est réussi, on
    // redirige vers la page de connexion
    window.location.replace('/connexion');
}
else if(response.status === 409) {
    // Afficher qu'il y a un conflit
}`;

const deconnexion =
`<form method="post" action="/deconnexion">
    <input type="submit" value="Deconnexion" />
</form>`;

export default function AuthUI() {
    return <>
        <section>
            <h2>Page client</h2>
            <p>
                Il est possible de tester les routes de connexion sur Postman, mais au final, nous voulons les
                utiliser avec une interface graphique. Pour nos routes, nous aurons simplement besoin de 2 pages. Il
                est toutefois possible de les mettre dans la même page. Voici nos 2 pages:
            </p>
            <ol>
                <li>Une page d'inscription</li>
                <li>Une page de connexion</li>
            </ol>
            <p>
                Dans ces 2 pages, nous devons simplement mettre un formulaire contenant un champ pour l'identifiant,
                un champ pour le mot de passe ainsi qu'un bouton de soumission. Vous pourriez utiliser un code HTML
                similaire à celui ci-dessous dans votre page de connexion.
            </p>
            <CodeBlock language="html">{form}</CodeBlock>
            <p>
                Le code HTML pour la page d'inscription serait très similaire. Nous aurions simplement à changer le
                texte dans le bouton de soumission et le <IC>id</IC> du formulaire. Vu ces similitudes, certains
                d'entre vous pourraient être assez ingénieux pour combiner ces 2 pages dans un seul
                fichier <IC>.handlebars</IC> lors de la génération de HTML serveur.
            </p>
        </section>

        <section>
            <h2>Appel des routes</h2>
            <p>
                Ces formulaires appèleront nos routes de façon très similaire à ce que nous avons vu jusqu'à présent
                dans le cours. Il faudra associé un fichier Javascript à la page HTML utilisé pour la connexion ou
                pour l'inscription et y mettre un code similaire à ce qu'il y a ci-dessous. Pour la page de connexion
                on pourrait avoir le code Javascript suivant:
            </p>
            <CodeBlock language="js">{fetching}</CodeBlock>
            <p>
                Pour la page d'inscription, on aurait un fichier Javascript similaire, mais on change le nom du
                formulaire et de la route. De plus, on doit modifier légèrement la section
                du <IC>response.ok</IC> puisqu'on doit gérer les erreur 409 lors de conflit au lieu des erreur 401 qui
                sont du à des problèmes de connexion.
            </p>
            <CodeBlock language="js">{fetchingInsc}</CodeBlock>
        </section>

        <section>
            <h2>Gérer la déconnexion</h2>
            <p>
                La déconnexion du serveur web est un peu particulière. En général, dans un site web, nous voulons
                simplement cliquer sur un bouton qui se retrouve souvent dans l'entête de la page pour se déconnecter.
                Puisque la route de déconnexion ne demande pas d'envoyer de paramètre nous pouvons la gérer uniquement
                avec du HTML et du CSS de la façon suivante:
            </p>
            <CodeBlock language="html">{deconnexion}</CodeBlock>
            <p>
                En effet, le formulaire est programmé pour automatiquement envoyé une requête à la route de
                déconnexion. Donc quand on clique sur le bouton, le formulaire est automatiquement envoyé, ce qui fait
                l'appel à notre route de déconnexion. Cette technique peut être utilisé pour toutes les requêtes qui
                n'ont pas besoin d'envoyer de données.
            </p>
        </section>
    </>
}