import React from 'react';
import Video from '../component/Video'
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-todo-serveur-solution.zip'

export default function LaboratoireTodoServeur() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                À partir du gabarit de projet de base d'express, construire un serveur de gestion de liste de tâche TODO. Votre 
                serveur doit supporter les fonctionnalités suivantes:
            </p>
            <ul>
                <li>Visualiser les TODO</li>
                <li>Ajouter des TODO</li>
                <li>Cocher des TODO</li>
            </ul>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Laboratoire - Serveur de liste TODO" src="https://www.youtube.com/embed/POJDsLz2R6s" />
        </section>

        <section>
            <h2>Solution</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
