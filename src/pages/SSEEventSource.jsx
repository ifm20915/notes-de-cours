import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const eventSource =
`let source = new EventSource('/nom-de-la-route')`;

const listeners = 
`source.addEventListener('move-played', (event) => {
    let data = JSON.parse(event.data);

    // Code à exécuter ici
};

source.addEventListener('player-drop', (event) => {
    let data = JSON.parse(event.data);

    // Code à exécuter ici
}`;

class SSEEventSource extends React.Component {
    render() {
        return <>
            <section>
                <h2>Ouverture d'une connexion à partir du client</h2>
                <p>
                    Si notre serveur est près à recevoir des connexions, il nous reste simplement à s'y connecter avec 
                    notre client. Pour ce faire, il existe un objet très pratique en Javascript, le <IC>EventSource</IC>.
                </p>
                <CodeBlock language="js">{ eventSource }</CodeBlock>
                <p>
                    Le constructeur de <IC>EventSource</IC> prend en paramètre la route vers laquelle notre connexion 
                    sera établie. Ce nouvel objet fera automatiquement la requête GET pour ouvrir la connexion.
                </p>
            </section>

            <section>
                <h2>Recevoir des données</h2>
                <p>
                    Une fois la connexion établie, le serveur peut nous envoyer des évènements à n'importe quel 
                    moment. Nous pouvons écouter ces évènements en ajoutant des listeners sur le <IC>EventSource</IC>.
                </p>
                <CodeBlock language="js">{ listeners }</CodeBlock>
                <p>
                    Vous pouvez ajouter des listeners sur plusieurs évènements et plusieurs listeners sur le même 
                    évènement.
                </p>
                <p>
                    Vous pouvez accéder aux données dans l'évènement dans l'attribut <IC>event.data</IC> du listener.
                    Si les données sont en JSON, assurez-vous de le convertir à nouveau en objet Javascript avant de 
                    les utiliser.
                </p>
                <p>
                    Il est important de noter que l'évènement que vous écoutez avec un listener dépend du 
                    champ <IC>event</IC> que vous envoyez à partir de votre serveur.
                </p>
            </section>
        </>;
    }
}

export default SSEEventSource;
