import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock';

import distibue from '../resources/laboratoire-14-distribué.zip';
import solution from '../resources/laboratoire-14-solution.zip';

const objectProp = 
`$premierPost = $posts[0];

// Affiche le message du premier post
echo $premierPost->message;`;

export default class Laboratoire14 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Avant de commencer ce laboratoire, suivez les étapes suivantes pour bien initialiser votre projet:
                </p>
                <ol>
                    <li>
                        Télécharger le fichier <IC>ditribué.zip</IC>.
                    </li>
                    <li>
                        Décompresser le fichier <IC>distribué.zip</IC> dans le dossier <IC>htdocs</IC> de votre 
                        serveur XAMPP.
                    </li>
                    <li>
                        Ouvrir le dossier décompressé dans votre éditeur de code favori.
                    </li>
                </ol>
                <p>
                    Dans ce projet, vous aurez une liste de publication faite sur un site de médias sociaux fictif. 
                    Ces publications se trouvent dans le fichier <IC>data.php</IC>. Vous devez modifier le 
                    fichier <IC>index.php</IC> pour qu'il répète la boîte de publication avec les bonnes données dans 
                    le HTML pour chaque publication qu'il y a dans le le tableau de publications.
                </p>
                <p>
                    Pour accéder à la propriété d'un objet en PHP, nous utilisons l'opérateur flèche <IC>-&gt;</IC> au 
                    lieu du point <IC>.</IC> comme la plupart des langages que vous connaisez.
                </p>
                <CodeBlock language="php">{ objectProp }</CodeBlock>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ distibue } name="distribué.zip"></DownloadBlock.File>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
};
