import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import DownloadBlock from '../component/DownloadBlock'

import solution from '../resources/laboratoire-librairie-solution.zip'

const livre = 
`{
    "isbn": "000-0000000000",
    "title": "Un titre",
    "nbPages": 12345,
    "summary": "Un résumé du livre",
    "authors": ["Un tableau", "des auteurs", "du livre"],
    "categories": ["Un tableau", "des catégories", "du livre"],
}`;

export default function LaboratoireLibrairie() {
    return <>
        <section>
            <h2>Marche à suivre</h2>
            <p>
                Programmez un serveur Web avec Node.js et Express qui répond aux besoins de la mise en situation 
                ci-dessous.
            </p>
            <p>
                Si vous désirez tester la solution, n'oubliez pas de faire un <IC>npm install</IC> sur le projet 
                au préalable pour qu'il télécharge les packages nécessaires. La solution contient une collection
                Postman pour vous aider à tester votre serveur.
            </p>
        </section>

        <section>
            <h2>Mise en situation</h2>
            <p>
                Vous voulez créer une site Web de présentation de livres. Selon vous, les livres devraient avoir 
                les attributs suivants:
            </p>
            <CodeBlock language="json">{ livre }</CodeBlock>
            <p>
                Pour la première version de votre serveur, vous voulez être capable de faire les actions suivantes:
            </p>
            <ul>
                <li>Lister le ISBN et le titre de tous les livre (dans la même requête)</li>
                <li>Ajouter un livre à votre banque de livre</li>
                <li>Chercher un livre par son ISBN dans la banque de livre</li>
                <li>Modifier un livre par son ISBN en lui donnant un nouveau livre pour le remplacer</li>
                <li>Supprimer un livre par son ISBN dans la banque de livre</li>
            </ul>
            <p>
                Votre système devrait être capable de valider les livres envoyés dans les requêtes d'ajout et de 
                modification. Assurez-vous que:
            </p>
        </section>

        <section>
            <h2>Solution et fichiers de départ</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
