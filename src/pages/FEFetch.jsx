import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'

const fetchExample = 
`// Faire la requête
let response = await fetch('/to-do-list');

// Convertir les données reçu en JSON
let data = await response.json()`;

const fetchOk = 
`let response = await fetch('/to-do-list');

if(response.ok){
    // Code ici
}`;

const fetchStatus = 
`let response = await fetch('/to-do-list');

if(response.status === 409){
    // Code de conflit ici
}
else if(response.status === 404){
    // Code de non trouvé ici
}
else if(response.status === 200){
    // Code OK ici
}`;

const fetchMethod = 
`let response1 = await fetch('/to-do-list', { method: 'POST' });
let response2 = await fetch('/to-do-list', { method: 'PUT' });
let response3 = await fetch('/to-do-list', { method: 'PATCH' });
let response4 = await fetch('/to-do-list', { method: 'DELETE' });`;

const fetchData = 
`// Données à envoyer
let data = { name: 'Faire le ménage' }

// Requête avec données
let response = await fetch('/to-do-list', { 
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
});`;

const xmlHttp = 
`let request = new XMLHttpRequest();
request.onreadystatechange = function() {
    // On regarde si la requête est fini et le code est OK
    if (this.readyState == 4 && this.status == 200) {
        // Code ici
    }
};

request.open('GET', 'localhost:1337/to-do-list');
request.send();`;

class FEFetch extends React.Component {
    render() {
        return <>
            <section>
                <h2>Faire une requête à partir du navigateur</h2>
                <p>
                    Jusqu'à présent, nous avons envoyé nos requêtes HTTP à l'aide de Postman pour tester nos serveurs. 
                    Toutefois, dans un cas réel, c'est souvent une application que vous construirez vous-même qui appelera 
                    le serveur. Dans ce cours, nous programmerons des applications web (un site web) pour qu'il 
                    communique avec notre serveur. À l'aide de la fonction Javascript <IC>fetch()</IC>, il est assez facile 
                    de créer nos propres requêtes HTTP dans notre site Web.
                </p>
                <p>
                    Cette fonction est uniquement disponible pour les navigateurs Web. Le code Javascript vu dans cette 
                    page ne peut donc pas être exécuté par Node.js. Toutefois, si vous mettez ce code dans la partie statique 
                    de votre serveur Web (dans le dossier <IC>public</IC>), ça va fonctionner puisque ce code est envoyé par 
                    le serveur, mais exécuté par le navigateur.
                </p>
                <p>
                    La fonction <IC>fetch()</IC> s'utilise de la façon suivante:
                </p>
                <CodeBlock language="js">{ fetchExample }</CodeBlock>
                <p>
                    Voici quelques détails important sur la fonction <IC>fetch()</IC>:
                </p>
                <ul>
                    <li>
                        La fonction <IC>fetch()</IC> ne nécessite pas l'ajout du domaine et du port comme avec Postman si on 
                        fait l'appel au même serveur que où la page web est hébergé. Bref, pour ce cours, vous n'aurez pas 
                        besoin de mettre le <IC>localhost:5000</IC> devant le chemin de vos requêtes.
                    </li>
                    <li><IC>fetch()</IC> retourne une promesse qui aura éventuellement la valeur de la réponse reçu</li>
                    <li>
                        Pour accéder aux données de la réponse, vous devez utiliser les fonctions <IC>json()</IC>, <IC>text()</IC> ou 
                        autres qui retourne eux-aussi une promesse
                    </li>
                    <li>
                        Puisque <IC>fetch()</IC> et les fonction d'accès aux données retournent des promesses, je vous 
                        recommande de l'utiliser avec <IC>await</IC>
                    </li>
                    <li>
                        Par défaut, <IC>fetch()</IC> fait un appel HTTP <IC>GET</IC> à l'adresse spécifié en paramètre
                    </li>
                </ul>
            </section>

            <section>
                <h2>Code de status</h2>
                <p>
                    Vous aurez à valider les codes de status HTTP pour voir si vos requêtes ont réussies ou non. 
                    Pour ce faire, vous pouvez utiliser la propriété <IC>ok</IC> pour regarder si la requête a 
                    été exécuté avec succès (code HTTP dans les 200).
                </p>
                <CodeBlock language="js">{ fetchOk }</CodeBlock>
                <p>
                    Vous pouvez aussi accéder directement au code de status directement avec la 
                    propriété <IC>status</IC> comme ceci:
                </p>
                <CodeBlock language="js">{ fetchStatus }</CodeBlock>
            </section>

            <section>
                <h2>Méthode HTTP</h2>
                <p>
                    Par défaut la fonction <IC>fetch()</IC> performe une requête HTTP de type <IC>GET</IC>. Si vous 
                    désirez changer la méthode HTTP, vous devrez configurer votre appel à <IC>fetch()</IC> de la façon 
                    suivante:
                </p>
                <CodeBlock language="js">{ fetchMethod }</CodeBlock>
                <p>
                    En général, avec la plupart des requêtes autre qu'un GET nécessiteront que vous envoyez des 
                    données. Vous pouvez ajouter la configuration suivante à <IC>fetch()</IC> pour y arriver:
                </p>
                <CodeBlock language="js">{ fetchData }</CodeBlock>
                <p>
                    Quelques détail sur le code ci-dessus:
                </p>
                <ul>
                    <li>
                        Par défaut, <IC>fetch()</IC> est configuré pour envoyer du texte. Puisque nous enverrons 
                        majoritairement des objets Javascript (JSON), nous devons changer le <IC>Content-Type</IC> de 
                        notre requête.
                    </li>
                    <li>
                        Les données doivent être envoyé sous forme de texte. C'est pourquoi nous convertissons nos 
                        données en chaîne de caractère JSON à l'aide de <IC>JSON.stringify()</IC>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Le vieux XMLHttpRequest</h2>
                <p>
                    Si vous vous trouvez dans un cas où vous devez développer pour des navigateurs plus vieux, il est 
                    tout de même possible de faire des requêtes HTTP à l'aide de l'objet <IC>XMLHttpRequest</IC>. Cet 
                    objet utilise les callbacks plutôt que les promesses et est définitivement plus difficile à 
                    utiliser que <IC>fetch()</IC>, mais c'est le prix à payer pour supporter les vieilles versions
                    d'Internet Explorer.
                </p>
                <CodeBlock language="js">{ xmlHttp }</CodeBlock>
                <p>
                    Vous pouvez en apprendre plus sur cette façon de faire les requêtes ici:
                </p>
                <p>
                    <a href="https://www.w3schools.com/xml/xml_http.asp" target="_blank" rel="noopener noreferrer">XML HttpRequest</a>
                </p>
            </section>
        </>;
    }
}

export default FEFetch;
