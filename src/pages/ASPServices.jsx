import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const record =
`namespace biblio.Models 
{
    public class Livre 
    {
        public string Isbn { get; set; }
        public string Titre { get; set; }
        public string Editeur { get; set; }
        public int NombrePages { get; set; }

        public Todo(string isbn, string titre, /* ... */){
            // Constructeur paramétré
            this.Isbn = isbn;
            this.Titre = titre;
            // ...
        }
    }
}`;

const contexte = 
`using System.Collections.Generic;

namespace biblio.Models 
{
    public class LivreContext 
    {
        // Liste d'objet de livres
        public List<Livre> ListeLivre { get; set; }

        public LivreContext(){
            // Initialiser la liste
            this.ListeLivre = new List<Livre>();

            // Ajouter des valeurs par défaut
            this.ListeLivre.Add(new Livre('0123456789', 'Un titre', 'Test', 351))
        }
    }
}`;

const services =
`public void ConfigureServices(IServiceCollection services)
{
    services.AddRazorPages();

    // Ligne à ajouter ici
    services.AddSingleton<LivreContext>();
}`;

const injection =
`// Créer un variable pour contenir une référence 
// de la liste de livre
public List<Livre> ListeLivre { get; set; }
        
public IndexModel(LivreContext livreContext){
    // Mettre une référence de la liste de livres 
    // du contexte directement dans une variable
    this.ListeLivre = livreContext.ListeLivre;
}`;

export default class ASPServices extends React.Component {
    render() {
        return <>
            <section>
                <h2>Créer un service</h2>
                <p>
                    Dans ASP.NET Core, un service est un peu comme un middleware dans Node.js. Un service nous permet 
                    donc de programmer des fonctionnalités pour plusieurs requêtes sur notre serveur. Les services les 
                    plus utilisé sont généralement les base de données, mais il en existe plusieurs autres.
                </p>
                <p>
                    Dans notre cas, ASP.NET, comme PHP, ne supporte pas par défaut la persistence de données par page.
                    La solution à ce problème est justement la création d'un service. Pour créer un service de
                    manipulation de données, nous aurons besoin de 2 choses:
                </p>
                <ul>
                    <li>Une classe définissant une données, appelé <IC>Record</IC></li>
                    <li>Une classe définissant l'accès aux données, appelé <IC>Contexte</IC></li>
                </ul>
                <p>
                    Ces 2 fichiers devront être ajouté dans un dossier <IC>Models</IC> dans votre projet. Le nom de 
                    ce dossier est un standard de Microsoft.
                </p>
                <p>
                    Pour le reste de la page, nous utiliserons un exemple un peu plus concret pour vous aider à 
                    comprendre, soit la gestion de livres dans une application Web. <IC>Record</IC> pourrait ressembler à ceci:
                </p>
            </section>

            <section>
                <h2>Record</h2>
                <p>
                    Pour la classe définissant la données, nous voulons sauvegarder des livres, donc nous devons nous 
                    faire une classe <IC>Livre</IC>. Cette classe n'a pas besoin d'être compliqué. En effet, vous 
                    n'avez besoin que de mettre une variable pour chaque champ d'un livre et un constructeur paramétré
                    pour pouvoir instancier des objets facilement.
                </p>
                <CodeBlock language="csharp">{ record }</CodeBlock>
            </section>

            <section>
                <h2>Contexte</h2>
                <p>
                    Le contexte est la classe qui contiendra l'ensemble de vos données. Généralement, vous voudrez les
                    sauvegarder dans une liste. La classe de contexte servira donc uniquement à contenir la liste 
                    ainsi que le code lui permettant de s'initialiser.
                </p>
                <CodeBlock language="csharp">{ contexte }</CodeBlock>
            </section>

            <section>
                <h2>Ajouter le contexte comme service</h2>
                <p>
                    Pour ajouter un contexte comme service dans votre application Web, vous devrez modifier très 
                    légèrement le fichier <IC>Startup.cs</IC>. En effet, tous les services sont enregistré dans la 
                    méthode <IC>ConfigureService()</IC> de ce fichier. Ajouter la ligne suivante à la fin de cette 
                    méthode vous permettra d'enregistrer votre contexte comme service:
                </p>
                <CodeBlock language="csharp">{ services }</CodeBlock>
                <p>
                    Une fois le service ajouté à votre projet, vous devrez l'injecter dans votre page. Ceci est fait 
                    en ajoutant un objet de la classe de contexte comme paramètre dans le constructeur de vos pages
                    qui vont l'utiliser.
                </p>
                <CodeBlock language="csharp">{ injection }</CodeBlock>
                <p>
                    Je vous suggère fortement l'approche ci-dessus dans vos pages. Le fait de sauvegarder directement 
                    une référence à la liste de livre à partir du contexte nous permettra de facilement y accéder dans 
                    le HTML et la manipuler facilement dans nos fonctions de requêtes.
                </p>
            </section>
        </>;
    }
};
