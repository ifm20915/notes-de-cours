import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const trust =
`const app = express();
app.enable('trust proxy');
// ...`;

const deploy =
`git add .
git commit -am "Deploiement sur Heroku"
git push heroku master`;

export default function Host() {
    return <>
        <section>
            <h2>Heroku</h2>
            <p>
                Heroku est une plateforme infonuagique (cloud) permettant d'héberger des applications. C'est l'une des
                rares plateforme à offrir de l'hébergement d'application gratuitement. Nous pourrons donc utiliser
                Heroku pour héberger nos serveurs. Les services gratuit sont toutefois limité à une certaine quantité
                d'heures d'utilisation et à certaines restrictions:
            </p>
            <ul>
                <li>
                    Vous pouvez mettre un maximum de 5 applications en ligne à la fois gratuitement. Si vous voulez en
                    mettre plus, vous devez vérifier votre compte avec une carte de crédit.
                </li>
                <li>
                    Les applications gratuite ont seulement 512MB de mémoire RAM. Si vous avez des applications qui
                    nécessite plus d'espace en mémoire vive, vous devez payer pour avec du meilleur matériel.
                </li>
                <li>
                    Un compte a droit à un maximum de 550 heures gratuites par mois. Donc l'ensemble de vos
                    applications sur cette plateforme peut être utiliser pour un maximum total de 550 heures avant
                    d'être fermé pour le restant du mois.
                </li>
                <li>
                    Une application ne recevant aucun traffic pendant environ 30 minutes sera automatiquement
                    endormie. Une application endormie n'utilise pas d'heures gratuite, ce qui est bien! Toutefoism si
                    un appel réveille l'application, il se peut que la requête prenne quelques secondes à s'exécuter.
                    Il faut laisser l'application le temps de se réveiller!
                </li>
            </ul>
            <p>
                Heroku est fait pourhéberger des applications. L'hébergement de base de données est faisable, mais
                plus complexe. Bref, si vous utiliser une base de données qui n'est pas intégré à votre application
                (comme SQLite par exemple), il sera difficile de faire votre configuration. Heroku offre toutefois un
                service d'hébergement gratuit de base de données avec PostgreSQL qui offre jusqu'à 1Go d'espace de
                disque. Nous ne couvrirons pas comment en faire l'utilisation, mais plusieurs tutoriels existent sur
                le web.
            </p>
        </section>

        <section>
            <h2>Préparation du projet</h2>
            <p>
                Pour héberger un projet sur Heroku, nous devons avant tout faire quelques modifications à notre
                projet:
            </p>
            <ol>
                <li>
                    <p>
                        La première étape, très simple, consiste à ajouter la ligne <IC>app.enable('trust proxy');</IC> à
                        votre fichier <IC>server.js</IC>:
                    </p>
                    <CodeBlock language="js">{trust}</CodeBlock>
                    <p>
                        Heroku lance les applications derrière un proxy inverse. Pour que notre application autorise ce
                        lancement, nous devons donc lui dire d'accepter le proxy avec la ligne ci-dessus.
                    </p>
                </li>
                <li>
                    <p>
                        La deuxième étape, aussi très simple, consiste à ajouter un fichier <IC>Profile</IC> à votre
                        projet. Vous devez donc ajouter un fichier nommé <IC>Procfile</IC> (Commençant par une lettre
                        majuscule et n'ayant pas d'extension) ayant le contenu suivant:
                    </p>
                    <CodeBlock language="procfile">{'web: npm run deploy'}</CodeBlock>
                    <p>
                        Ce fichier indiquera à Heroku que nous utilisons une application web et que la commande pour
                        le lancer est <IC>npm run deploy</IC>. Cette commande démarre notre serveur, mais sans
                        utiliser Nodemon, l'utilitaire pour redémarrer le serveur en développement.
                    </p>
                </li>
            </ol>
            <p>
                C'est tout! Votre projet est maintenant prêt à être héberger sur Heroku!
            </p>
        </section>

        <section>
            <h2>Héberger votre application</h2>
            <p>
                Avant d'héberger votre application, vous devez faire 2 choses:
            </p>
            <ol>
                <li>
                    Créer un compte sur le site web de <a href="https://signup.heroku.com/login" target="_blank" rel="noopener noreferrer">
                        Heroku
                    </a>.
                </li>
                <li>
                    Installer le bon <a href="https://devcenter.heroku.com/articles/heroku-cli" target="_blank" rel="noopener noreferrer">
                        Heroku CLI
                    </a> (Command-Line Interface) pour votre système d'exploitation.
                </li>
            </ol>
            <p>
                Une fois ces procédures faites, nous pourrons héberger notre application en suivant les étapes suivantes:
            </p>
            <ol>
                <li>
                    Se connecter à <a href="https://id.heroku.com/login" target="_blank" rel="noopener noreferrer">
                        Heroku
                    </a> sur le web.
                </li>
                <li>
                    Cliquer sur le bouton <IC>New</IC> pour ajouter une nouvelle application.
                </li>
                <li>
                    Ajouter un nom unique à votre application. Garder le nom de l'application tout proche puisqu'il
                    est utile pour les prochaines étapes
                </li>
                <li>
                    Ouvrir un terminal dans votre projet.
                </li>
                <li>
                    Se connecter à Heroku dans le terminal en exécutant la commande suivante et en suivant les
                    instructions dans le terminal.
                    <CodeBlock language="shell">{'heroku login'}</CodeBlock>
                </li>
                <li>
                    Si votre projet n'utilise pas Git, lancer la commande d'initialisation de Git.
                    <CodeBlock language="shell">{'git init'}</CodeBlock>
                </li>
                <li>
                    Connecter Heroku avec votre projet à l'aide de Git à l'aide de la commande suivante et en
                    changeant le nom du projet pour le nom de votre projet.
                    <CodeBlock language="shell">{'heroku git:remote -a nom-du-projet'}</CodeBlock>
                </li>
                <li>
                    Déployer votre application en lançant les commandes Git ci-dessous. Lors du <IC>push</IC>, si la
                    commande ne fonctionne pas, essayer de changer <IC>master</IC> pour <IC>main</IC>.
                    <CodeBlock language="shell">{deploy}</CodeBlock>
                </li>
                <li>
                    Dans l'interface web de Heroku, cliquer sur le nom de votre application et rendez-vous dans
                    ses <IC>Settings</IC>.
                </li>
                <li>
                    Dans l'interface de configuration, cliquer sur le bouton <IC>Reveal Config Vars</IC>.
                </li>
                <li>
                    Ajouter les variables du fichier <IC>.env</IC> de votre projet à cet endroit. Vous n'avez pas
                    besoin d'ajouter le <IC>PORT</IC>, n'y le <IC>NODE_ENV</IC> puisqu'ils sont respectivement mis
                    par Heroku aux valeurs <IC>443</IC> et <IC>production</IC> par défaut. Vous aurez donc
                    généralement seulement le <IC>DB_FILE</IC> et le <IC>SESSION_SECRET</IC> à mettre.
                </li>
                <li>
                    Cliquer sur le bouton <IC>Open app</IC> pour ouvrir et tester votre application. Si l'application
                    ne fonctionne pas, vous pouvez essayer de cliquer sur <IC>More</IC> et <IC>Restart all dynos</IC> pour
                    redémarrer l'application.
                </li>
            </ol>
        </section>

        <section>
            <h2>Fermer une application</h2>
            <p>
                Si vous voulez fermer une application pour être certain qu'elle n'utilise pas d'heure gratuite, vous
                pouvez le faire en exécutant les opérations suivantes:
            </p>
            <ol>
                <li>Sur Heroku, cliquer sur votre application et ensuite aller dans l'onglet <IC>Resources</IC>.</li>
                <li>
                    Cliquer sur le bouton avec le petit crayon à côté de la commande de déploiement.
                </li>
                <li>
                    Cliquer sur l'interrupteur pour fermer l'application et cliquer sur <IC>Confirm</IC>.
                </li>
            </ol>
            <p>
                Si vous voulez plutôt supprimer une application définitivement, faites les opérations suivantes:
            </p>
            <ol>
                <li>Sur Heroku, cliquer sur votre application et ensuite aller dans l'onglet <IC>Settings</IC>.</li>
                <li>Cliquer sur le bouton <IC>Delete app...</IC> et suivre les instructions.</li>
            </ol>
        </section>
    </>
}
