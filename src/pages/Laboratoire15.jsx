import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import distibue from '../resources/laboratoire-15-distribué.zip';
import solution from '../resources/laboratoire-15-solution.zip';

export default class Laboratoire15 extends React.Component {
    render() {
        return <>
            <section>
            <h2>Marche à suivre</h2>
                <p>
                    Avant de commencer ce laboratoire, suivez les étapes suivantes pour bien initialiser votre projet:
                </p>
                <ol>
                    <li>
                        Télécharger le fichier <IC>ditribué.zip</IC>.
                    </li>
                    <li>
                        Décompresser le fichier <IC>distribué.zip</IC> dans le dossier <IC>htdocs</IC> de votre 
                        serveur XAMPP.
                    </li>
                    <li>
                        Ouvrir le dossier décompressé dans votre éditeur de code favori.
                    </li>
                </ol>
                <p>
                    Dans ce projet, nous programmerons la liste TODO que nous avons fait avec Node.js, mais en PHP. Il 
                    y aura aussi quelques différences. Voici les points importants pour réussir ce laboratoire:
                </p>
                <ul>
                    <li>
                        L'interface graphique et le Javascript sont déjà fait.
                    </li>
                    <li>
                        Vous devez programmer les routes qui sont utilisé par le client dans le fichier <IC>index.php</IC> qui 
                        se trouve dans le dossier <IC>todo</IC>.
                    </li>
                    <li>
                        Si vous voulez utiliser la classe <IC>Todo</IC>, n'oubliez pas de l'inclure dans votre fichier PHP.
                    </li>
                    <li>
                        Vous devez faire la validation des données reçues sur le serveur. La validation sur le client n'est pas nécessaire.
                    </li>
                    <li>
                        Les données des TODO doivent être stocké dans la session de l'utilisateur.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Téléchargement</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ distibue } name="distribué.zip"></DownloadBlock.File>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
};
