import React from 'react';
import CodeBlock from '../component/CodeBlock';
import IC from '../component/InlineCode';

const npmMiddleware = 
`$ npm i helmet
$ npm i compression
$ npm i cors`;

const expressMiddleware = 
`// Importation de Express et de plusieurs middlewares
import express, { json } from 'express';
import helmet from 'helmet';
import compression from 'compression';
import cors from 'cors';

// Création du serveur
const app = express();

// Ajout de middlewares
app.use(helmet());
app.use(compression());
app.use(cors());
app.use(json());

// Démarrage du serveur
const PORT = 2345;
app.listen(PORT);`;

export default function BackendMiddleware() {
    return <>
        <section>
            <h2>Installation de middlewares</h2>
            <p>
                Par défaut, Express ne fait pas grand chose et n'offre pas beaucoup de fonctionnalités. C'est 
                pourquoi nous installerons quelques middlewares à Express. Un middleware est une fonction ou un 
                ensemble de fonction qui s'exécuteront sur les requêtes reçus ou les réponse envoyés par votre serveur 
                Express. Certains de ces middlewares s'exécutent sur toutes les requêtes et réponses alors que 
                d'autres ne s'exécutent que sous certaines conditions.
            </p>
            <p>
                Pour installer un middlewares, nous utiliserons la commande suivante: 
            </p>
            <CodeBlock language="shell">$ npm i nom_middleware</CodeBlock>
            <p>
                Pour installer tous les middlewares nécessaires pour l'instant, vous devrez donc lancer les commandes 
                suivantes dans votre projet: 
            </p>
            <CodeBlock language="shell">{ npmMiddleware }</CodeBlock>
        </section>
        <section>
            <h2>Ajouter des middlewares</h2>
            <p>
                Pour utiliser ces middlewares dans Express, nous devons les importer avec un <IC>import</IC>, mais 
                aussi utiliser la fonction <IC>app.use</IC> de la façon suivante:
            </p>
            <CodeBlock language="js">{ expressMiddleware }</CodeBlock>
            <p>
                Vous noterez que les middlewares doivent être intégrer dans le serveur express  avec <IC>app.use</IC> avant
                le démarrage du serveur avec <IC>app.listen</IC>.
            </p>
        </section>

        <section>
            <h2>Description des middlewares</h2>
            <p>
                Voici la liste des middlewares que nous utiliserons pour le moment ainsi que leur utilité:
            </p>
            <table>
                <tr>
                    <th>Nom</th><th>Description</th><th>Nécessite autres installations</th>
                </tr>
                <tr>
                    <td>helmet</td>
                    <td>
                        Permet de sécuriser notre serveur face à différentes attaques HTTP connues. En développement, 
                        ce middleware n'est pas très utile, mais lorsque notre application est mise en production sur 
                        le web, ce middleware devient vraiment important.
                    </td>
                    <td>Oui</td>
                </tr>
                <tr>
                    <td>compression</td>
                    <td>
                        Permet compresser et décompresser les requêtes et les réponses si possible pour envoyer 
                        des données moins volumineuse sur le réseau, donc plus rapidement.
                    </td>
                    <td>Oui</td>
                </tr>
                <tr>
                    <td>cors</td>
                    <td>
                        Permet d'activer les requêtes pour le Cross-Origin Resource Sharing (CORS). Ce type de 
                        requête est envoyé lorsque nous utilisons plusieurs serveurs pour héberger le site Web et
                        le serveur Web. En développement, ces requêtes ne seront pas utilisé, mais une fois en 
                        production, ces requêtes pourraient être un problème. Il est donc bien d'ajouter ce 
                        middleware pour sécuriser notre serveur.
                    </td>
                    <td>Oui</td>
                </tr>
                <tr>
                    <td>json</td>
                    <td>
                        Permet de lire les données envoyées en JSON dans une requête HTTP. Sans ce middleware, vous ne 
                        pourrez pas lire et traiter les données envoyées à notre serveur. Nous utiliserons donc 
                        presque toujours ce middleware.
                    </td>
                    <td>Non</td>
                </tr>
            </table>
            <p>
                Il existe de nombreux autres middlewares. Vous pouvez en trouver une bonne quantité si vous 
                visitez le lien suivant:
            </p>
            <p>
                <a href="http://expressjs.com/en/resources/middleware.html" target="_blank" rel="noopener noreferrer">Express middleware</a>
            </p>
        </section>
    </>;
}
