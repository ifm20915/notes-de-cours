import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const productsGetExpress =
`app.get('/products', async (request, response) => {
    let data = await products.get();
    response.status(200).json(data);
});`;

const productsAddExpress =
`app.post('/products', async (request, response) => {
    if(!validateProduct(request.body)){
        response.sendStatus(400);
    }
    else{
        products.add(
            request.body.productCode,
            request.body.productName,
            request.body.description,
            request.body.standardCost,
            request.body.listPrice
        );
        response.sendStatus(200);
    }
});`;

export default function DBExpress() {
    return <>
        <section>
            <h2>Utilisation dans les routes</h2>
            <p>
                Dans votre serveur Express, si vous voulez accéder aux données retournées par un requête SQL, vous 
                vous trouverez face à un problème: votre fonction qui fait la requête SQL retourne une promesse.
                Vous devrez donc vous assurer que la fonction de la route que vous utilisez dans votre 
                fichier <IC>server.js</IC> soit elle aussi une fonction <IC>async</IC> et d'utiliser 
                le <IC>await</IC> si vous aller chercher des données.
            </p>
            <CodeBlock language="js">{ productsGetExpress }</CodeBlock>
            <p>
                Si vous voulez plutôt faire la route pour ajouter les données, votre code pourrait ressembler à 
                ceci:
            </p>
            <CodeBlock language="js">{ productsAddExpress }</CodeBlock>
        </section>
    </>;
}