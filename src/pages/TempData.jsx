import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const simple =
`<h1>Titre</h1>
<p>
    Lorem ipsum, dolor sit amet consectetur 
    adipisicing elit. Perspiciatis culpa 
    amet hic, temporibus veritatis vel 
    possimus nulla iusto. Alias vel 
    voluptatum provident blanditiis 
    exercitationem corrupti ullam debitis 
    voluptates aliquam possimus.
</p>`;

const render =
`app.get('/', (request, response) => {
    response.render('home');
});`;

const renderData =
`app.get('/', (request, response) => {
    response.render('home', {
        titre: 'Accueil',
        texte: 'Ceci est un petit paragraphe de texte',
        nombre: 42
    });
});`;

const htmlVar =
`<h1>{{titre}}</h1>

<p>{{texte}}</p>

<input type="number" value="{{nombre}}">`;

const renderDB =
`app.get('/', async (request, response) => {
    // Dans ce cas-ci, la fonction getTodos()
    // nous renvoit les TODO qui proviennent
    // d'une base de données
    let todos = await getTodos();

    response.render('/', {
        titre: 'Liste TODO',
        todos: todos
    });
});`;

export default function TempData() {
    return <>
        <section>
            <h2>Génération d'une page</h2>
            <p>
                Pour générer une page HTML, nous devrons tout d'abords créer un autre fichier <IC>.handlebars</IC> qui
                va contenir le contenu de notre page. Pour l'instant, nous pourrions simplement créer le
                fichier <IC>home.handlebars</IC> suivant:
            </p>
            <CodeBlock language="handlebars">{simple}</CodeBlock>
            <p>
                Comme vous pouvez le constater, ce fichier ne contient pas tout le code habituel d'un fichier HTML.
                Ceci est dû au fait que ce fichier sera fusionné avec le fichier <IC>main.handlebars</IC>. En effet,
                le fichier <IC>main.handlebars</IC> est le fichier de base de votre page HTML et le code du
                fichier <IC>home.handlebars</IC> y sera inséré à la place du <IC>{'{{{body}}}'}</IC>. Nous verrons
                plus tard comment utiliser ce concept à notre avantage.
            </p>
            <p>
                La prochaine étape consiste à retourner le HTML généré dans une route sur le serveur. Pour ce faire,
                nous créerons une route <IC>GET</IC> sur le serveur qui va retourner un rendu du HTML que nous avons
                fait jusqu'à présent.
            </p>
            <CodeBlock language="js">{render}</CodeBlock>
            <p>
                La ligne <IC>response.render('home');</IC> indique que nous retournons en réponse un rendu HTML da la
                page <IC>home.handlebars</IC>. Vous pourrez donc maintenant démarrer votre serveur et tester cette
                nouvelle route en ouvrant votre navigateur à la route créé. Dans ce cas-ci, puisque nous avons créé
                la route racine <IC>/</IC>, vous n'avez qu'à ouvrir le navigateur sur votre serveur sans spécifier de
                route particulière.
            </p>
            <ColoredBox heading="Attention">
                Dans l'exemple ci-dessus, nous retournons le HTML sur la route <IC>/</IC>. Pour être certain de ne pas
                avoir de conflit avec les fichiers statiques, assurez-vous de supprimer le fichier <IC>index.html</IC> dans
                le dossier <IC>public</IC>. De toute façon, à partir de maintenant, vous allez probablement générer
                tous vos fichiers HTML avec Handlebars, donc les fichiers HTML dans le dossier <IC>public</IC> ne
                seront plus nécessaire.
            </ColoredBox>
        </section>

        <section>
            <h2>Ajouter des données</h2>
            <p>
                Jusqu'à présent, notre génération de HTML ne fait pas grand chose de plus qu'un fichier statique de
                HTML. Nous allons donc ajouter des données à notre page pour pouvoir y changer son affichage en
                conséquence. Pour ajouter des données, nous devons tout d'abords les ajouter lors du rendu de la façon
                suivante:
            </p>
            <CodeBlock language="js">{renderData}</CodeBlock>
            <p>
                Nous ajoutons donc un objet contenant des variables à la fonction de génération. Ces variables peuvent
                être de n'importe quel type et seront passées au fichier <IC>home.handlebars</IC> et <IC>main.handlebars</IC> pour
                que l'on puisse les utiliser dans le HTML.
            </p>
            <p>
                Dans les fichiers <IC>.handlebars</IC>, vous pouvez utiliser la syntaxe <IC>{'{{nomDeVariable}}'}</IC> pour
                afficher une variable passé à la fonction de génération. Vous pouvez utiliser cette syntaxe dans le
                contenu ou même dans les attributs HTML. Voici un exemple du fichier <IC>home.handlebars</IC> avec les
                données ci-dessus:
            </p>
            <CodeBlock language="handlebars">{htmlVar}</CodeBlock>
            <p>
                Nous utilisons ici comme données des variables très simple pour nos données, mais il est tout à fait
                possible de passer des données venant d'une base de données ou d'une autre source à nos fichiers <IC>.handlebars</IC>.
            </p>
            <CodeBlock language="js">{renderDB}</CodeBlock>
            <p>
                Pour voir plus d'information sur comment manipuler les données lors de la génération du HTML avec
                Handlebars, consulter le lien suivants:
            </p>
            <p>
                <a href="https://handlebarsjs.com/guide/expressions.html" target="_blank" rel="noopener noreferrer">
                    Expressions
                </a>
            </p>
        </section>
    </>;
}
