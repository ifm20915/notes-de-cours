import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox';

const phpHello = 
`<?php 
    $nom = "Wilkie";
    $prenom = "Jonathan";
    echo "Bonjour " . $prenom . " " . $nom;
?>`;

export default class PHPIntro extends React.Component {
    render() {
        return <>
            <section>
                <h2>Historique</h2>
                <p>
                    PHP est un langage de programmation principalement utilisé pour le scripting. Même si son site Web 
                    indique qu'il peut être utilisé pour autre chose, il est principalement utilisé dans la 
                    programmation de serveurs Web, conjointement avec le serveur Web statique Apache.
                </p>
                <p>
                    Ce n'est pas un jeune langage de programmation. Il a été créé en 1995, environ en même temps que 
                    le Javascript. Le PHP a toutefois moins bien vieilli et bien qu'il soit toujours maintenu à jour,
                    les programmeurs l'utilisent de moins en moins, principalement en faveur d'autres technologies 
                    comme Node.js ou ASP.NET Core.
                </p>
                <p>
                    PHP est l'acronyme récursif PHP: Hypertext Preprocessor. Ce nom est très logique puisque nous 
                    utiliserons ce langage principalement pour générer des page d'hypertext, plus communément appelé 
                    des pages HTML, de façon dynamique par un serveur.
                </p>
            </section>

            <section>
                <h2>Apache</h2>
                <p>
                    Pour utiliser PHP dans ce cours, nous utiliserons un serveur Apache avec PHP installé en 
                    complément. Pour ce faire, je vous suggère d'utiliser un serveur XAMPP. Si vous démarrez le 
                    service Apache dans XAMPP, le module de PHP sera automatiquement démarré.
                </p>
                <p>
                    Pour commencer le développement avec PHP, vous aurez besoin de connaître le dossier de votre 
                    serveur Web Apache. Si vous utilisez XAMPP, vous le trouverez ici:
                </p>
                <ul>
                    <li>
                        Sur Windows: <IC>C:\xampp\htdocs</IC>
                    </li>
                    <li>
                        Sur Mac (???): <IC>/Applications/XAMPP/xamppfiles/htdocs/</IC>
                    </li>
                </ul>
                <p>
                    Pour vous créer un projet que vous pourrez tester, vous devez créer un dossier avec le nom de 
                    votre projet à l'intérieur du dossier <IC>htdocs</IC>. Vous pourrez ensuite ouvrir ce dossier avec 
                    votre éditeur de code favori.
                </p>
            </section>

            <section>
                <h2>Fichier de code PHP</h2>
                <p>
                    Dans votre dossier de projet, nous allons créer un fichier de code PHP. Vous pouvez nommer ce 
                    fichier n'importe comment, tant que son extension est <IC>.php</IC>. Je vous suggère toutefois 
                    d'appeler votre fichier PHP principal <IC>index.php</IC>.
                </p>
                <p>
                    Dans votre fichier PHP, vous ne pouvez pas simplement commencer à écrire du PHP. Il faut 
                    absolument mettre des balises spéciales pour que le langage sache que vous écrivez du PHP.
                </p>
                <CodeBlock language="php">{ phpHello }</CodeBlock>
                <p>
                    Comme vous pouvez le voir dans l'exemple ci-dessus, tout code PHP doit être entouré de la 
                    balise <IC>&lt;?php ... ?&gt;</IC>. Si la balise n'est pas là, le code PHP ne sera pas exécuté.
                </p>
            </section>

            <section>
                <h2>Tester votre application</h2>
                <p>
                    Si votre serveur Apache est ouvert, vous pourrez tester automatiquement votre code PHP en ouvrant 
                    votre navigateur et en accédant à l'adresse suivante:
                </p>
                <ul>
                    <li>
                        Sur Windows: <IC>localhost/nom_projet/nom_fichier.php</IC>
                    </li>
                    <li>
                        Sur Mac: <IC>localhost:8080/nom_projet/nom_fichier.php</IC>
                    </li>
                </ul>
                <p>
                    Si vous avez nommé votre nom de fichier <IC>index.php</IC>, vous pourrez raccourcir l'URL dans 
                    votre navigateur pour omettre le nom du fichier PHP.
                </p>
                <ColoredBox heading="À noter">
                    Si votre fichier s'appelle <IC>index.php</IC>, le serveur Apache pourra y accéder plus facilement.
                    En effet, dans un serveur Apache, si un fichier s'appelle <IC>index.html</IC> ou <IC>index.php</IC> dans
                    un dossier, c'est ce fichier là qui sera retourné par défaut si on accède au dossier.
                </ColoredBox>
                <p>
                    Si tout fonctionne correctement, vous verrez dans votre navigateur une page qui 
                    écrira <IC>Bonjour Jonathan Wilkie</IC>, ou peu importe quel autre nom vous aurez mis dans le code 
                    PHP.
                </p>
            </section>
        </>;
    }
};
