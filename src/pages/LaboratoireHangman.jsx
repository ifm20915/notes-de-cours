import React from 'react';
import DownloadBlock from '../component/DownloadBlock';

import distribue from '../resources/laboratoire-hangman-distribué.zip';
import solution from '../resources/laboratoire-hangman-solution.zip';

export default function LaboratoireHangman() {
    return <>
        <section>
            <h2>Mise en situation</h2>
            <p>
                Vous devez programmer un jeu de bonhomme pendu qui va chercher un mot aléatoirement dans un 
                dictionnaire français et le fait deviner au joueur.
            </p>
            <p>
                Voici quelques spécifications supplémentaires:
            </p>
            <ul>
                <li>Le joueur a un nombre illimité de chances</li>
                <li>Le jeu s'arrête lorsque le joueur a trouvé le mot</li>
                <li>Lorsque le jeu s'arrête, le nombre de coups utilisé est affiché</li>
            </ul>
        </section>

        <section>
            <h2>Fonctions supplémentaires</h2>
            <p>
                Si vous voulez vous pratiquer, n'hésitez pas à essayer d'ajouter les éléments suivant au jeu:
            </p>
            <ul>
                <li>Permettre au joueur de recommencer une nouvelle partie s'il le désire</li>
                <li>Donner un nombre maximal de chance au joueur avant qu'il perde</li>
                <li>Afficher un visuel en caractère avec de la couleur dans la console</li>
            </ul>
        </section>

        <section>
            <h2>Solution et fichiers de départ</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ distribue } name="distribué.zip"></DownloadBlock.File>
                <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>;
}
