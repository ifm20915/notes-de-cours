import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const gettype = 
`function isChampValid($champ){
    return gettype($champ) === 'string';
}`;

const isString = 
`function isChampValid($champ){
    return is_string($champ);
}`;

const otherValidation =
`function isNombreValid($nombre){
    return is_int($nombre) && 
        $nombre >= 0 && 
        $nommbre <= 100;
}

function isChaineValid($chaine){
    return is_string($chaine) && 
        strlen($chaine) > 0 &&
        strlen($chaine) < 255;
}`;

const regexValidation = 
`function isCodePostalValid($codePostal){
    return is_string($codePostal) && 
        preg_match(
            '/^[A-Za-z]\\d[A-Za-z][ -]?\\d[A-Za-z]\\d$/',
            $codePostal
        ) === 1
}`;

const routeValidation =
`include './validation.php';

$body = json_decode(file_get_contents('php://input'), true);

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(!isChampValid($body['champ'])){
        http_response_code(400);
        echo 'Bad request';
    }
    else{
        // Code si la validation est correcte
    }
}`;

export default class PHPValidation extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    La validation en PHP est très similaire à celle que l'on fait dans un serveur Node.js. Dans un but 
                    de ne pas mettre trop de code dans un seul fichier, je vous recommande de vous créer un fichier de 
                    validations pour chaque fichier de route que vous créez. Ainsi, si vous avez une 
                    route <IC>/user/order</IC>, vous avez créé un fichier <IC>index.php</IC> dans un 
                    dossier <IC>order</IC>, dans un dossier <IC>user</IC>. Vous allez donc créer le 
                    fichier <IC>validation.php</IC> dans le même dossier que le <IC>index.php</IC> ci-dessus.
                </p>
                <p>
                    Dans ce fichier <IC>validation.php</IC>, nous mettrons toutes les fonctions de validations 
                    nécessaires pour valider les données arrivant sur le serveur. Comme pour un serveur Node.js, un 
                    serveur PHP doit être très rigide sur sa validation pour prévenir le piratage. Nous devrons donc 
                    créer une fonction de validations pour chaque champ reçu dans notre requête.
                </p>
            </section>

            <section>
                <h2>Validation du type</h2>
                <p>
                    Pour chaque champ à valider, la première validation à faire sera de vérifer si le type de la 
                    variable est bon. Pour ce faire, nous utiliserons la fonction <IC>gettype</IC> qui nous retournera 
                    une chaîne avec le type de variable
                </p>
                <CodeBlock language="php">{ gettype }</CodeBlock>
                <p>
                    Il est aussi possible d'utiliser des fonctions de validations de type pour la validation. Ces 
                    fonctions ressemblent à ceci:
                </p>
                <CodeBlock language="php">{ isString }</CodeBlock>
                <p>
                    Pour plus d'information sur ces fonctions, je vous suggère de consulter le lien suivant:
                </p>
                <a href="https://www.php.net/manual/en/function.gettype.php" target="_blank" rel="noopener noreferrer">
                    gettype
                </a>
            </section>

            <section>
                <h2>Autres validations</h2>
                <p>
                    En général, vous aurez d'autres validations à faire sur les champs que seulement la validation du 
                    type. Par exemple, pour un nombre, vous pourriez vouloir qu celui-ci soit dans un certain 
                    interval. Pour une chaîne de caractère, c'est généralement la taille nécessite une validation.
                </p>
                <CodeBlock language="php">{ otherValidation }</CodeBlock>
                <p>
                    Pour une chaîne de caractère, il est aussi parfois voulu de regarder si elle suit un certain 
                    patron. Nous pouvons utiliser les expressions régulière (regex) pour ce genre de cas. En PHP, 
                    c'est la fonction <IC>preg_match()</IC> qui nous permettra de faire cette validation.
                </p>
                <CodeBlock language="php">{ regexValidation }</CodeBlock>
            </section>

            <section>
                <h2>Utilisation de la validation dans vos routes</h2>
                <p>
                    L'utilisation de la validation est assez simple. N'oubliez toutefois pas d'ajouter 
                    le <IC>include</IC> avant d'utiliser vos fonctions de validations. Lorsqu'il est temps de brancher 
                    votre validation dans vos routes, vous pouvez utiliser un code similaire à celui-ci:
                </p>
                <CodeBlock language="php">{ routeValidation }</CodeBlock>
            </section>
        </>;
    }
};
