import React from 'react';
import CodeBlock from '../component/CodeBlock';
import ColoredBox from '../component/ColoredBox'
import IC from '../component/InlineCode';

const expressStart = 
`import express from 'express';

/** Création du serveur */
let app = express();

// Démarrage du serveur
const PORT = 1337;
app.listen(PORT);`;

const dotenv = 
`PORT=5000
NODE_ENV=development`;

const dotenvConfig =
`// Aller chercher les configurations de l'application
import 'dotenv/config'`;

export default function BackendExpress() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Express est une librairie de code permettant de facilement créer des serveurs Web efficace sur 
                Node.js. Nous utiliserons cette librairie dans le cours. Même si la base de Express est assez 
                facile à utiliser, c'est une librairie qui utilise des notions complexes du protocole HTTP. Si 
                vous voulez plus d'information sur cette librairie de code, je vous suggère fortement d'aller 
                voir sa documentation officielle:
            </p>
            <p>
                <a href="https://expressjs.com/" target="_blank" rel="noopener noreferrer">Express</a>
            </p>
        </section>

        <section>
            <h2>Créer une application Express</h2>
            <p>
                Pour utiliser Express, vous devez premièrement l'installer. Pour ce faire, entrez la commande 
                suivante dans votre terminal:
            </p>
            <CodeBlock language="shell">$ npm i express</CodeBlock>
            <p>
                Par la suite, vous pourrez commencer à écrire le code de base de votre application dans votre 
                fichier <IC>server.js</IC>. Voici un squelette de code de base pour commencer à utiliser Express:
            </p>
            <CodeBlock language="js">{ expressStart }</CodeBlock>
            <p>
                Voici quelques explications sur ce code:
            </p>
            <dl>
                <dt><IC>import express from 'express'</IC></dt>
                <dd>
                    Pour utiliser une librairie de code, nous devons utiliser le mot-clé <IC>import</IC>. La 
                    librairie de code de Express nous expose une fonction que nous pourrons utiliser pour créer
                    notre serveur Web.
                </dd>

                <dt><IC>const app = express();</IC></dt>
                <dd>
                    Crée un objet de serveur que nous pourrons configurer dans les prochaines étapes.
                </dd>

                <dt><IC>app.listen(PORT);</IC></dt>
                <dd>
                    Démarre le serveur Web. Celui-ci écoutera les requêtes sur le port spécifié en paramètre. 
                    L'exécution de cette fonction doit se faire après que votre serveur soit configuré.
                </dd>
            </dl>
        </section>

        <section>
            <h2>Fichier de configuration</h2>
            <p>
                Pour simplifier notre travail dans le futur, je vous recommande fortement de mettre les configurations 
                de Express et de Node.js dans un fichier externe. Ceci nous permet de facilement modifier certains 
                paramètres de votre serveur sans avoir à modifier le code du projet. Pour ce faire, nous créerons un 
                fichier nommé <IC>.env</IC> dans votre projet qui contiendra les configurations suivantes:
            </p>
            <CodeBlock language="properties">{ dotenv }</CodeBlock>
            <p>
                Ces 2 propriétés nous permettent de configurer le serveur de la façon suivante:
            </p>
            <dl>
                <dt>PORT</dt>
                <dd>
                    Permet de définir le port utilisé par Express lorsque nous lançons le serveur.
                </dd>

                <dt>NODE_ENV</dt>
                <dd>
                    Permet d'indiquer à Node.js comment il doit lancer l'application. Par défaut, nous mettrons la
                    valeur <IC>development</IC> qui est déjà la valeur par défaut de Node.js. Lorsque vous mettrez 
                    votre serveur en ligne, pour des raisons de performance et de sécurité, nous voudrons mettre la 
                    valeur <IC>production</IC>.
                </dd>
            </dl>
            <p>
                Plus tard, nous ajouterons d'autre configuration dans ce fichier.
            </p>
        </section>

        <section>
            <h2>Utilisation des configurations</h2>
            <p>
                Pour accéder à ces valeurs dans le code, nous devrons utiliser une autre librairie de code sur 
                Node.js qui se nomme <IC>dotenv</IC>. Vous pouvez l'installer de la façon suivante:
            </p>
            <CodeBlock language="shell">$ npm i dotenv</CodeBlock>
            <p>
                Pour l'utiliser dans votre code, vous devez l'importer et la configurer de la façon ci-dessous:
            </p>
            <CodeBlock language="js">{ dotenvConfig }</CodeBlock>
            <ColoredBox heading="Attention">
                Cette ligne de code doit absolument être au début de votre fichier principal pour que les configurations 
                soient chargées au lancement du serveur.
            </ColoredBox>
            <p>
                Pour accéder aux valeurs du fichier <IC>.env</IC> dans votre code, vous pourrez utiliser le code 
                suivant:
            </p>
            <CodeBlock language="js">process.env.NOM_VARIABLE</CodeBlock>
            <p>
                Dans notre cas, si nous voulons utiliser le port dans le fichier <IC>.env</IC>, nous changerons le 
                démarrage du serveur de la façon suivante:
            </p>
            <CodeBlock language="js">app.listen(process.env.PORT);</CodeBlock>
        </section>
    </>;
}
