import React from 'react';
import IC from '../component/InlineCode';
import Situation from '../component/Situation'

export default function BackendPlan() {
    return <>
        <section>
            <h2>Définitions</h2>
            <p>
                Un API (Application Programming Interface) est un ensemble de fonctions et procédures qui 
                expose ou donne accès à des fonctionnalités ou des données pour des applications ou des 
                services.
            </p>
            <p>
                Dans notre cas, nous programmerons un API serveur, donc un ensemble de fonctions et procédures 
                qui permettront aux navigateurs Web, notre client, de faire des requêtes pour accéder à des 
                données ou des services.
            </p>
        </section>

        <section>
            <h2>Les cas d'utilisation</h2>
            <p>
                La planification d'un API serveur est généralement assez facile. C'est en fait un cas typique de 
                cas d'utilisation (use/case). Il existe même des standards de diagrammes pour développer des cas 
                d'utilisation, mais on n'ira pas jusque là dans ce cours. Dans notre cas, on aura simplement à se 
                demander:
            </p>
            <p>
                Quelles actions pourront être accompli par les clients utilisant mon serveur?
            </p>
            <p>
                Voici un exemple pour nous aider:
            </p>
            <Situation>
                On veut créer un système où les utilisateurs peuvent créer ou supprimer des salles de clavardage.
                Si une salle de clavardage existe un utilisateur pourra la rejoindre. Si un utilisateur est dans 
                une salle de clavardage, il pourra envoyer des messages ou quitter la salle de clavardage. 
                Finalement, on veut que les utilisateurs puissent voir les salles de clavardage présentement 
                disponible et aussi voir les utilisateurs qui sont présent dans un salle de clavardages.
            </Situation>
            <p>
                Dans cette situation, notre serveur a besoin de sauvegarder les données sur les salles de 
                clavardage et de permettre aux utilisateurs de faire les actions suivantes:
            </p>
            <ul>
                <li>Lister les salles de clavardage</li>
                <li>Créer une salle de clavardage</li>
                <li>Supprimer une salle de clavardage</li>
                <li>Lister les utilisateur dans une salle de clavardage</li>
                <li>Joindre une salle de clavardage</li>
                <li>Sortir d'une salle de clavardage</li>
                <li>Envoyer un message dans une salle de clavardage</li>
            </ul>
            <p>
                Déterminer cette liste de fonctionnalités est l'une des étapes les plus importantes pour la création 
                de votre serveur. Il sera possible de modifier ou d'ajouter des éléments à cette liste plus tard, 
                mais si votre planification est bien faite, vous aurez beaucoup plus de facilité à programmer votre 
                serveur.
            </p>
        </section>

        <section>
            <h2>Définir les routes</h2>
            <p>
                Notre API serveur utilisera le protocole HTTP. Dans ce protocole, c'est les routes qui définissent les 
                fonctionnalités. Nous voulons donc convertir chacun des cas d'utilisation ci-dessus en une route pour 
                notre serveur.
            </p>
            <p>
                Pour chacun des cas, nous listerons la route, la méthode HTTP, les données envoyées dans la 
                requête du client ainsi que les données retournées par les réponses du serveur. Nous allons aussi 
                spécifier quel façon d'envoyer les données nous utiliserons.
            </p>
            <p>
                Avec l'exemple ci-dessus, nous pourrions avoir les propriétés suivantes:
            </p>
            <table>
                <tr>
                    <th>Cas d'utilisation</th><th>Route</th><th>Méthode HTTP</th><th>Données requête</th><th>Données réponse</th>
                </tr>
                <tr>
                    <td>Lister les salles de clavardage</td>
                    <td>/salles</td>
                    <td>GET</td>
                    <td>Rien</td>
                    <td><IC>String[]</IC> Tableau des noms des salles de clavardage</td>
                </tr>
                <tr>
                    <td>Créer une salle de clavardage</td>
                    <td>/salles</td>
                    <td>POST</td>
                    <td><IC>String</IC> Nom de la salle à créer (body)</td>
                    <td>Rien</td>
                </tr>
                <tr>
                    <td>Supprimer une salle de clavardage</td>
                    <td>/salles</td>
                    <td>DELETE</td>
                    <td><IC>String</IC> Nom de la salle à supprimer (body)</td>
                    <td>Rien</td>
                </tr>
                <tr>
                    <td>Lister les utilisateur dans une salle de clavardage</td>
                    <td>/salles/:nom&#8209;de&#8209;salle</td>
                    <td>GET</td>
                    <td><IC>String</IC> Nom de la salle de clavardage dont nous voulons lister les utilisateurs (route)</td>
                    <td><IC>String[]</IC> Tableau des noms d'utilisateur dans la salle de clavardage</td>
                </tr>
                <tr>
                    <td>Joindre une salle de clavardage</td>
                    <td>/salles/:nom&#8209;de&#8209;salle</td>
                    <td>POST</td>
                    <td>
                        <div><IC>String</IC> Nom de la salle à joindre (route)</div>
                        <div><IC>String</IC> Nom de l'utilisateur à joindre à la salle (body)</div>
                    </td>
                    <td>Rien</td>
                </tr>
                <tr>
                    <td>Sortir d'une salle de clavardage</td>
                    <td>/salles/:nom&#8209;de&#8209;salle</td>
                    <td>DELETE</td>
                    <td>
                        <div><IC>String</IC> Nom de la salle à joindre (route)</div>
                        <div><IC>String</IC> Nom de l'utilisateur à sortir de la salle (body)</div>
                    </td>
                    <td>Rien</td>
                </tr>
                <tr>
                    <td>Envoyer un message dans une salle de clavardage</td>
                    <td>/salles/:nom&#8209;de&#8209;salle</td>
                    <td>POST</td>
                    <td>
                        <div><IC>String</IC> Nom de la salle à joindre (route)</div>
                        <div><IC>String</IC> Nom de l'utilisateur qui écrit le message (body)</div>
                        <div><IC>String</IC> Message à envoyer dans la salle (body)</div>
                    </td>
                    <td>Rien</td>
                </tr>
            </table>
            <p>
                Vous noterez que certaines routes nécessiteront l'envoi de données dans la requête de multiples façons 
                différentes. C'est des cas qui arrivent relativement fréquemments. Dans tout les cas, c'est vous qui 
                décidez comment vous voulez envoyer les données. Oui, il est généralement plus facile de les envoyer dans 
                le corps de la requête HTTP (body), mais n'oubliez pas qu'il y a des restrictions avec la méthode <IC>GET</IC>.
            </p>
            <p>
                Certaines personnes vont ajouter des propriétés supplémentaires à leurs actions, comme les 
                différents codes de status HTTP qu'il est possible de recevoir en exécutant l'action ainsi 
                que les conditions qui causeront le retour de ces codes. Nous n'irons pas jusque là ici, mais il 
                n'est pas mauvais de planifier ce genre de propriétés non plus.
            </p>
            <p>
                Une fois toutes les propriété défini, il ne nous reste qu'à programmer notre API dans notre 
                serveur.
            </p>
        </section>
    </>;
}
