import CodeBlock from "../component/CodeBlock";
import IC from "../component/InlineCode";

const baseJs = 
`let bouton = document.querySelector('#formulaire input[type=submit]');
let message = document.querySelector('#formulaire .message');
bouton.addEventListener('click', () => {
    if(formulaireEstValide()) {
        message.style.backgroundColor = 'green';
        message.innerText = 'Formulaire envoyé!'
        envoyer();
    }
    else{
        message.style.backgroundColor = 'red';
        message.innerText = 'Vous avez une erreur dans votre formulaire';
    }
};`;

export default function RevisionJS() {
    return <>
        <section>
            <h2>Dans un navigateur web</h2>
            <p>
                Le Javascript est nécessaire à n'importe quel page Web 
                plus complexe puisqu'il nous permet de définir des 
                comportements dans notre page page. Un cas typique serait 
                simplement de programmer un comportement lorsqu'un 
                utilisateur clique sur un bouton:
            </p>
            <CodeBlock language="js">
                { baseJs }
            </CodeBlock>
            <p>
                Le Javascript est un langage de programmation fonctionnel,
                c'est-à-dire qu'il nous permet facilement de manipuler les 
                fonctions comme si c'était des variables. De la même 
                façon, Javascript est un langage de programmation objet 
                puisqu'il qu'il nous permet de facilement manipuler des 
                objets. De plus, ce langage est toujours en développement 
                et des changements surviennent à chaque année, bien que  
                la plupart d'entre eux ne soient pas toujours supporté par 
                les différents engins à la sortie. Toutes ces 
                caractéristiques font du Javascript l'un des langages de 
                programmation le plus complexe.
            </p>
            <p>
                Bien que nous ferons beaucoup de Javascript dans ce cours,
                je vous recommande fortement d'être à l'aise avec les 
                bases. Si ce n'est pas le cas, je vous recommande 
                fortement la lecture des sites Web suivant:
            </p>
            <p>
                <a href="https://developer.mozilla.org/fr/docs/Learn/JavaScript/First_steps" target="_blank" rel="noopener noreferrer">Premiers pas en JavaScript</a>
            </p>
            <p>
                <a href="https://developer.mozilla.org/fr/docs/Apprendre/JavaScript/Building_blocks" target="_blank" rel="noopener noreferrer">Principaux blocs en JS</a>
            </p>
        </section>

        <section>
            <h2>DOM</h2>
            <p>
                Pour bien utiliser le Javascript dans le navigateur Web, 
                il est aussi important de connaître en partie 
                le <strong>Domain Object Model</strong> (DOM). Le DOM est 
                la représentation en mémoire de notre page Web et il nous 
                est possible de le lire ou le modifier à l'aide du 
                Javascript. Si vous n'êtes pas à l'aise avec les bases du 
                DOM, je vous recommande de survoler le tutoriel suivant:
            </p>
            <p>
                <a href="https://www.w3schools.com/js/js_htmldom.asp" target="_blank" rel="noopener noreferrer">JavaScript HTML DOM</a>
            </p>
        </section>

        <section>
            <h2>Bonnes pratiques</h2>
            <p>
                Les bonnes pratiques du langage Javascript sont similaires à celle des langages de la famille du C 
                (C, C++, C#, Java). En voici une liste non exhaustive:
            </p>
            <ul>
                <li>
                    Mettre un commentaire au-dessus de chaque variable de classe ou de module pour indiquer ce qu'elle 
                    contient.
                </li>
                <li>
                    Mettre un commentaire de documentation au-dessus de chaque fonction indiquant ce qu'elle prends en 
                    paramètre, ce qu'elle fait et ce qu'elle retourne.
                </li>
                <li>
                    Mettre des commentaires à tout endroit dans le code où la logique n'est pas évidente à comprendre 
                    en un coup d'oeil rapide.
                </li>
                <li>
                    Utiliser le <IC>camelCase</IC> commençant par une lettre minuscule pour le nom des variables et le 
                    nom des fonctions.
                </li>
                <li>
                    Nommer les variables, fonctions et classes clairement.
                </li>
                <li>
                    Utiliser le <IC>CamelCase</IC> commençant par une lettre majuscule pour le nom des classes et 
                    constructeur.
                </li>
                <li>
                    Toujours mettre un point-virgule <IC>;</IC> à la fin de vos instructions.
                </li>
                <li>
                    Indenter correctement votre code avec des tabulations ou des espaces.
                </li>
            </ul>
        </section>
    </>
}