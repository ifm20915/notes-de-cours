import React from 'react';
import IC from '../component/InlineCode'
import CodeBlock from '../component/CodeBlock'
import ColoredBox from '../component/ColoredBox'

import sseDiagram from '../resources/sse.svg'
import Video from '../component/Video';

const openConnection = 
`app.get('/nom-de-la-route', (request, response) => {
    // Retourner le stream au client
    response.writeHead(200, {
        'Cache-Control': 'no-cache',
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive'
    });
});`;

const connectionStayAlive = 
`app.get('/nom-de-la-route', (request, response) => {
    // Retourner le stream au client
    response.writeHead(200, {
        'Cache-Control': 'no-cache',
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive'
    });

    // Boucle pour garder la connexion en vie
    const intervalId = setInterval(() => {
        response.write(':\\n\\n');
        response.flush();
    }, 30000);
});`;

const connectionClose = 
`app.get('/nom-de-la-route', (request, response) => {
    // Retourner le stream au client
    response.writeHead(200, {
        'Cache-Control': 'no-cache',
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive'
    });

    // Boucle pour garder la connexion en vie
    const intervalId = setInterval(() => {
        response.write(':\\n\\n');
        response.flush();
    }, 30000);
    
    // Arrêter la boucle si le client stop la connexion
    response.on('close', () => {
        clearInterval(intervalId);
        response.end();
    });
});`;

const keepResponse = 
`// Variable pour contenir la connexion
let connexion = null;

app.get('/nom-de-la-route', (request, response) => {
    response.writeHead(200, {
        'Cache-Control': 'no-cache',
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive'
    });

    // Stocker la connexion
    connexion = response;

    // ...
});`;

const writeData = 
`let data; // some data
connexion.write(data);
connexion.flush();`;

const dataFormat = 
`event: nom-de-evenement
data: Données à envoyer au client sous la forme de texte

`;

const dataJson = 
`// Convertir les données en chaîne
let data = { nom: 'Jonathan', alias: 'Batman' };
let dataString = 
    'event: new-connection\\n' + 
    'data: ' + JSON.stingify(data) + '\\n\\n';

// Écrire les données
connexion.write(data);
connexion.flush();`;

export default function SSEBackend() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Jusqu'à présent, lorsque nous avons des communications entre le client et le serveur, le serveur 
                répond uniquement aux requêtes du client. Le serveur ne peut pas envoyer d'informations à un 
                client sans avoir reçu au préalable une requête. Il est toutefois possible d'avoir un certain 
                intermédiaire avec les Server Sent Event (SSE).
            </p>
            <p>
                Voici leur fonctionnement:
            </p>
            <ol>
                <li>Le client ouvre une connexion vers le serveur</li>
                <li>Tant que la connexion reste ouverte, le serveur peut envoyer des évènements (des données) sur cette connexion</li>
                <li>Le client peut réagir aux données qui arrive sur la connexion</li>
                <li>À n'importe quel moment, le client ou le serveur peut fermer la connexion</li>
            </ol>
            <img src={ sseDiagram } alt="Fonctionnement des Server Sent Events"/>
        </section>

        <section>
            <h2>Ouverture de connexion dans Express</h2>
            <p>
                Express supporte les Server Sent Event, mais il demande un peu plus de configuration qu'à 
                l'habitude. En général, pour établir la connexion, le client utilisera simplement une requête HTTP 
                GET. Il faut donc programmer cette requête pour qu'elle retourne un flux de données (data stream).
            </p>
            <CodeBlock language="js">{openConnection}</CodeBlock>
            <p>
                Il faudra aussi ajouter à cette requête GET un moyen de la garder en vie. En effet, si aucune 
                donnée n'est envoyé sur le stream pendant un certain temps, le navigateur Web fermera en général 
                la connexion automatiquement, ce qui pourrait nous causer des problèmes. Nous ajouterons donc une 
                boucle qui envoie périodiquement des données vide au client pour que la connexion reste ouverte.
            </p>
            <CodeBlock language="js">{connectionStayAlive}</CodeBlock>
            <p>
                Il sera finalement nécessaire d'ajouter un morceau de code qui arrêtera la boucle d'envoie 
                automatique si le client décide de fermer la connexion. Nous en profiterons aussi fermer la 
                connexion du côté du serveur.
            </p>
            <CodeBlock language="js">{connectionClose}</CodeBlock>
            <ColoredBox heading="À noter">
                Nous n'avons pas encore vu les instructions <IC>setInterval</IC> et <IC>clearInterval</IC>. Voici 
                ce qu'elle font:
                <dl>
                    <dt>setInterval</dt>
                    <dd>
                        Répète la fonction spécifié en paramètre après un certain interval de temps aussi spécifié
                        en paramètre. La fonction exécuté est toujours asynchrone. Cette fonction retourne un 
                        identifiant qui nous permet de l'arrêter plus tard.
                    </dd>
                    <dt>clearInterval</dt>
                    <dd>
                        Permet d'arrêter une boucle d'exécution asynchrone qui a été démarrer avec <IC>setInterval</IC>.
                        Cette fonction nécessite l'identifiant d'interval que nous voulons arrêter.
                    </dd>
                </dl>
            </ColoredBox>
        </section>

        <section>
            <h2>Envoyer des données</h2>
            <p>
                Pour envoyer des données dans le flux (stream), nous devons avoir accès à l'objet de réponse que 
                nous avons utilisé pour établir la connexion. Je vous suggère donc de le stocker dans une variable 
                sur votre serveur.
            </p>
            <CodeBlock language="js">{keepResponse}</CodeBlock>
            <p>
                Une fois la connexion stocké dans une variable, vous pourrez envoyer des données dessus à 
                n'importe quel moment en utilisant la fonction <IC>write</IC>.
            </p>
            <CodeBlock language="js">{writeData}</CodeBlock>
            <ColoredBox heading="Attention">
                Si vous regarder des exemples sur le Web le <IC>flush</IC> est rarement là. Dans notre cas, il est 
                nécessaire puisque nous utilisons le middleware de compression. Sans l'instruction <IC>flush</IC>, 
                les données ne seront pas envoyée.
            </ColoredBox>
        </section>

        <section>
            <h2>Format des données</h2>
            <p>
                Les données envoyé sur un stream sont des chaînes de caractères ayant un format particulier. Ces 
                chaînes doivent suivre le format suivant:
            </p>
            <CodeBlock language="shell">{dataFormat}</CodeBlock>
            <p>
                Chaque donnée envoyé nécessite au moins le champ <IC>data</IC>, mais je vous recommande fortement
                de toujours mettre le champ event aussi. 
            </p>
            <ColoredBox>
                Il existe d'autres champs, mais ils sont plus technique. Vous pouvez en apprendre plus au lien suivant:
                <p>
                    <a href="https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events#Event_stream_format" target="_blank" rel="noopener noreferrer">
                        Event stream format
                    </a>
                </p>
            </ColoredBox>
            <p>
                Voici quelques autres informations importantes:
            </p>
            <ul>
                <li>Chaque champ doit se terminer par un retour de ligne (<IC>\n</IC>)</li>
                <li>
                    S'il y a des retours dans un champ <IC>data</IC>, il faut utiliser plusieurs fois le 
                    champ <IC>data</IC> pour indiquer que celui-ci change de ligne</li>
                <li>L'envoie de données doit toujours se terminer par 2 retours de ligne (<IC>\n\n</IC>)</li>
            </ul>
            <p>
                Si vous désirez envoyer du JSON, vous pouvez utiliser <IC>JSON.stringify</IC> pour le transformer 
                en chaîne de caractère.
            </p>
            <CodeBlock language="js">{dataJson}</CodeBlock>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Server Sent Event (SSE) - Partage de données en temps réel" src="https://www.youtube.com/embed/eP6D9bybN-Y" />
        </section>
    </>;
}
