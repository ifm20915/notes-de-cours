import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import Video from '../component/Video';

const config =
`// ...

// Importer les fichiers et librairies
// ...
import { engine  } from 'express-handlebars';

// Création du serveur
const app = express();
app.engine('handlebars', engine());
app.set('view engine', 'handlebars');
app.set('views', './views');

// Ajout de middlewares
// ...`;

const main =
`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Titre</title>

    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    {{{body}}}

    <script type="module" src="./js/main.js"></script>
</body>
</html>`;

export default function TempHandlebars() {
    return <>
        <section>
            <h2>Introduction</h2>
            <p>
                Handlebars est l'un des nombreux engins de génération de HTML, que l'on appelle aussi engin de rendus
                HTML. Ces engins nous permettent de générer du HTML à partir de données. On l'utilise fréquement pour
                répéter certaines sections du HTML automatiquement ou encore pour cacher ou afficher certaines parties
                du HTML en fonction des données.
            </p>
            <p>
                Dans notre cas, nous utiliserons Handlebars pour générer le HTML sur notre serveur Express. Nous
                utiliserons donc la librairie <IC>express-handlebars</IC> que vous pouvez télécharger dans votre
                projet avec la commande suivante:
            </p>
            <CodeBlock language="shell">{'npm install express-handlebars'}</CodeBlock>
            <p>
                Il exsite de nombreuses implémentation de Handlebars dans Express, mais celle que vous installerez
                avec la commande ci-dessus est la plus populaire et la plus facile à utiliser.
            </p>
        </section>

        <section>
            <h2>Configurations</h2>
            <p>
                Pour utiliser Handlebars dans Express, nous devons faires quelques configurations. La première
                consiste à créer une nouvelle structure de dossier dans notre projet. À la racine de votre projet,
                vous devrez créer un dossier <IC>views</IC> dans lequel vous mettrez les dossiers <IC>layouts</IC> et <IC>partials</IC>.
                Vous créerez aussi le fichier <IC>main.handlebars</IC> que vous mettrez dans le dossier <IC>layouts</IC>.
            </p>
            <p>
                À la fin, vous devez avoir une structure similaire à ceci dans votre dossier de projet:
            </p>
            <ul className="overflow-protection" style={{ listStyleType: 'none', marginLeft: '0', padding: '1rem', border: '2px solid var(--border-interactive-color)'}}>
                <li>
                    <span style={{ display: 'flex', alignItems: 'center' }}>
                        <span class="material-icons">folder</span>
                        dossier-de-projet
                    </span>
                    <ul style={{ listStyleType: 'none', marginLeft: '12px', paddingLeft: '1rem', borderLeft: '2px dashed var(--border-interactive-color)'}}>
                        <li>
                            <span style={{ display: 'flex', alignItems: 'center' }}>
                                <span class="material-icons">folder</span>
                                public
                            </span>
                            <ul style={{ listStyleType: 'none', marginLeft: '12px', paddingLeft: '1rem', borderLeft: '2px dashed var(--border-interactive-color)'}}>
                                <li>...</li>
                            </ul>
                        </li>
                        <li>
                            <span style={{ display: 'flex', alignItems: 'center' }}>
                                <span class="material-icons">folder</span>
                                views
                            </span>
                            <ul style={{ listStyleType: 'none', marginLeft: '12px', paddingLeft: '1rem', borderLeft: '2px dashed var(--border-interactive-color)'}}>
                                <li>
                                    <span style={{ display: 'flex', alignItems: 'center' }}>
                                        <span class="material-icons">folder</span>
                                        layouts
                                    </span>
                                    <ul style={{ listStyleType: 'none', marginLeft: '12px', paddingLeft: '1rem', borderLeft: '2px dashed var(--border-interactive-color)'}}>
                                        <li style={{ display: 'flex', alignItems: 'center' }}>
                                            <span class="material-icons">insert_drive_file</span>
                                            main.handlebars
                                        </li>
                                    </ul>
                                </li>
                                <li style={{ display: 'flex', alignItems: 'center' }}>
                                    <span class="material-icons">folder</span>
                                    partials
                                </li>
                            </ul>
                        </li>
                        <li style={{ display: 'flex', alignItems: 'center' }}>
                            <span class="material-icons">insert_drive_file</span>
                            .env
                        </li>
                        <li style={{ display: 'flex', alignItems: 'center' }}>
                            <span class="material-icons">insert_drive_file</span>
                            server.js
                        </li>
                        <li>...</li>
                    </ul>
                </li>
            </ul>
            <p>
                De plus, dans votre fichier <IC>server.js</IC>, vous devez ajouter les lignes de code ci-dessous. Ces
                lignes crée une instance de l'engin de rendu de HTML, l'insère dans Express et le définisse comme
                engin de rendu par défaut.
            </p>
            <CodeBlock language="js">{config}</CodeBlock>
        </section>

        <section>
            <h2>Fichier <IC>main.handlebars</IC></h2>
            <p>
                Le fichier <IC>main.handlebars</IC> contiendra le code de base des pages HTML de toutes les pages dans
                votre site web. En effet, une bonne quantité de vos pages utiliserons les mêmes éléments, comme par
                exemple l'entête ou le pied de votre page.
            </p>
            <p>
                Nous verrons comment configurer ce fichier plus tard, mais pour l'instant, vous pouvez simplement y
                mettre le code suivant:
            </p>
            <CodeBlock language="handlebars">{main}</CodeBlock>
        </section>

        <section>
            <h2>Autres engins de rendu</h2>
            <p>
                Il existe de nombreux autres engins de rendu. En voici une courte liste qui fonctionne avec Express:
            </p>
            <ul>
                <li>
                    <a href="https://pugjs.org/api/getting-started.html" target="_blank" rel="noopener noreferrer">
                        Pug
                    </a>
                </li>
                <li>
                    <a href="https://www.npmjs.com/package/mustache" target="_blank" rel="noopener noreferrer">
                        Mustache
                    </a>
                </li>
                <li>
                    <a href="https://ejs.co/" target="_blank" rel="noopener noreferrer">
                        EJS
                    </a>
                </li>
            </ul>
            <p>
                Nous avons décidé d'utiliser Handlebars dans le cours puisqu'il gardait la syntaxe de base du HTML et
                que l'intégration des données se faisait de façon très similaire à d'autres plateformes de
                programmation web avancé que vous verrez dans d'autres cours. En fait, il se base sur l'engin Mustache,
                présent dans la liste ci-dessus, qui est utilisé comme base pour plusieurs engin de génération de
                code. Vous y trouverez donc des similitudes avec plusieurs autres librairies de code.
            </p>
        </section>

        <section>
            <h2>Vidéo</h2>
            <Video title="Handlebars - Installation et configuration" src="https://www.youtube.com/embed/Ud2v0nyZlio?si=uOzTPGVrMYuoXwBg" />
        </section>
    </>;
}
