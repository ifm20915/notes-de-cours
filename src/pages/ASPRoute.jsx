import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';
import Situation from '../component/Situation';

const controller = 
`using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using todo_list.Models;

namespace todo_list.Controllers
{
    [Route("nom-de-route")]
    [ApiController]
    public class TodoListController : ControllerBase
    {   
        public TodoListController(ContexteCommeService contexte)
        {
            // ...
        }
    }
}`;

const httpMethod = 
`[HttpPost]
public ActionResult<ObjetARetourner> NomDeFonction(ObjetARecevoir obj)
{
    if(!validationQuelconque())
    {
        return BadRequest();
    }

    // Code de la route ici
    // ...

    // Valeur à retourner avec le code HTTP
    return donneeARetourner;
}`;

const httpParam = 
`[HttpGet("{id}")]
public ActionResult<Livre> GetLivre(int id)
{
    Livre livreTrouve = rechercherLeLivre(id);
    if(livreTrouve == null)
    {
        return NotFound();
    }
    else
    {
        return livreTrouve;
    }
}`;

const plugController =
`public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    // ...

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapRazorPages();

        // Ligne à ajouter ci-dessous:
        endpoints.MapControllers();
    });
}`;

export default class Laboratoire18 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Introduction</h2>
                <p>
                    L'approche par formulaire de ASP.NET Core possède une faiblesse majeure:
                </p>
                <p>
                    À chaque fois que l'on envois un requête au serveur, la page doit se recharger, ce qui implique 
                    un regénération de la page sur le serveur et l'envois de beaucoup de données sur le réseau. 
                </p>
                <p>
                    Une approche plus efficace serait que le serveur renvoit uniquement les données nécessaires au 
                    navigateur Web et que celui-ci s'occupe d'afficher les données dans son interface. C'est 
                    essentiellement le principe que nous utilisons avec Node.js et PHP. Heureusement, ASP.NET Core 
                    nous permet de créer nos propres routes lui-aussi, comme en PHP et en Node.js. Pour ce faire, nous 
                    devrons créer un controlleurs.
                </p>
            </section>

            <section>
                <h2>Controlleur</h2>
                <p>
                    Le controlleur va être une classe qui nous permettra de programmer des routes. Je vous suggère de 
                    vous créer un dossier <IC>Controllers</IC> dans votre projet où vous pourrez mettre plusieurs 
                    controlleurs. Le gabarit de base d'un controlleur ressemble à ceci:
                </p>
                <CodeBlock language="csharp">{ controller }</CodeBlock>
                <p>
                    Voici quelques informations sur ce code qui peuvent vous être utile à savoir:
                </p>
                <ul>
                    <li>
                        L'annotation <IC>[ApiController]</IC> indique que ce controlleur offre des routes.
                    </li>
                    <li>
                        L'annotation <IC>[Route("nom-de-route")]</IC> indique le nom de la route utilisé pour ce controlleur.
                    </li>
                    <li>
                        Le controlleur doit toujours hérité de la classe <IC>ControllerBase</IC>.
                    </li>
                    <li>
                        Vous pouvez injecter des services et des contextes dans le contructeur, comme pour les 
                        contructeurs de pages Razor sur le serveur.
                    </li>
                    <li>
                        En général, un controlleur supporte une seule route. Si vous en voulez plusieurs, il faut 
                        créer plusieurs controlleurs.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Méthode HTTP</h2>
                <p>
                    Dans le controlleur, vous allez programmer les différentes méthodes HTTP pour votre route. Ces 
                    méthodes seront programmé sous forme de fonction et auront la forme suivante:
                </p>
                <CodeBlock language="csharp">{ httpMethod }</CodeBlock>
                <p>
                    Voici quelques informations sur ce code qui peuvent vous être utile à savoir pour la création de 
                    ces fonctions:
                </p>
                <ul>
                    <li>
                        L'annotation au dessus de la fonction indique la méthode HTTP supporté par la fonction. Les 
                        valeurs accepté sont généralement <IC>[HttpGet]</IC>, <IC>[HttpPost]</IC>, <IC>[HttpPut]</IC>, <IC>[HttpPatch]</IC> et <IC>[HttpDelete]</IC>.
                    </li>
                    <li>
                        Votre fonction doit toujours retourner un <IC>ActionResult</IC>. Si vous voulez retourner des 
                        données avec cet appel HTTP, vous devez spécifier le nom de la classe représentant les données 
                        entre les symboles <IC>&lt;</IC> et <IC>&gt;</IC>. Si la requête HTTP ne doit rien retourner, 
                        vous n'avez pas besoin de mettre le <IC>&lt;</IC> et le <IC>&gt;</IC>
                    </li>
                    <li>
                        Si votre requête HTTP doit recevoir des données, vous devez mettre un objet de la classe 
                        représentant ces données en paramètre.
                    </li>
                    <li>
                        <p>
                            Pour retourner un <IC>ActionResult</IC>, vous pouvez utiliser les méthodes de base disponible 
                            dans le controlleur. Vous pouvez les trouver au lien suivant:
                        </p>
                        <p>
                            <a href="https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.controllerbase?view=aspnetcore-5.0" target="_blank" rel="noopener noreferrer">
                                ControllerBase Class
                            </a>
                        </p>
                        <p>
                            Si vous ne voulez pas vous casser la tête, vous pouvez simplement utiliser les 
                            fonctions <IC>NotFound()</IC>, <IC>BadRequest()</IC> ou retourner directement vos données
                            dans le return.
                        </p>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Paramètre HTTP</h2>
                <p>
                    Vous avez la situation suivante:
                </p>
                <Situation>
                    <p>
                        Vous voulez que votre route puisse chercher le livre dans votre base de données ayant un ID 
                        spécifié dans l'URL. Par exemple, si l'utilisateur veut le livre ayant l'ID 42, il utiliserait la 
                        route de la façon suivante: 
                    </p>
                    <IC>GET /livre/42</IC>
                </Situation>
                <p>
                    Les données passé dans l'URL sont appelé des paramètres. Nous pouvons programmer ce genre de cas 
                    facilement en ASP.NET Core faisant quelques petites modifications à nos fonctions:
                </p>
                <CodeBlock language="csharp">{ httpParam }</CodeBlock>
                <p>
                    Vous noterez les détails suivants:
                </p>
                <ul>
                    <li>
                        On indique dans l'annotation de la fonction que l'on veut recevoir un paramètre ID dans l'URL.
                    </li>
                    <li>
                        Dans les arguments de la fonction, on ajoute une variable qui va contenir la valeur reçu du 
                        paramètre.
                    </li>
                </ul>
            </section>

            <section>
                <h2>Brancher le controlleur dans votre application</h2>
                <p>
                    Même si vous créez un controlleur, il ne sera pas ajouté à l'application par défaut. Vous devrez 
                    indiquer à ASP.NET Core d'ajouter ces routes. Pour ce faire, nous devons ajouter une petite ligne 
                    de code dans le fichier <IC>Startup.cs</IC> dans la fonction <IC>Configure()</IC>:
                </p>
                <CodeBlock language="csharp">{ plugController }</CodeBlock>
                <p>
                    Avec cette petite ligne de code, votre controlleur devrait maintenant fonctionner. Vous pourrez 
                    donc tester vos routes avec Postman et faire des <IC>fetch()</IC> à partir de votre client.
                </p>
            </section>
        </>;
    }
};
