import React from 'react';
import CodeBlock from '../component/CodeBlock';
import IC from '../component/InlineCode'

const moduleClosure = 
`// Fichier module.js
const module = (() => {
    let variablePrivee = 'Je suis privée';

    let fonctionPrivee = () => {
        return 'Je suis privée aussi';
    };

    return {
        variablePublique: 'Je suis publique',

        fonctionPublique: () => {
            return 'Je suis aussi publique'
        }
    }
})();`;

const moduleClosureMain = 
`// Fichier main.js
let resultat = module.fonctionPublique();
resultat += module.variablePublique;`;

const moduleCommonJs = 
`// Fichier module.js
let variablePrivee = 'Je suis privée';

let fonctionPrivee = () => {
    return 'Je suis privée aussi';
};

exports.variablePublique = 'Je suis publique';

exports.fonctionPublique = () => {
    return 'Je suis aussi publique'
};`;

const moduleCommonJsMain = 
`// Fichier main.js
const module = require('./module');

let resultat = module.fonctionPublique();
resultat += module.variablePublique;`;

const moduleEC6 = 
`// Fichier module.js
let variablePrivee = 'Je suis privée';
let variablePublique = 'Je suis publique';

let fonctionPrivee = () => {
    return 'Je suis privée aussi';
};
let fonctionPublique = () => {
    return 'Je suis aussi publique'
};

export { variablePublique };
export { fonctionPublique };`;

const moduleEC6Main = 
`// Fichier main.js
import { variablePublique, fonctionPublique } from './module';

let resultat = fonctionPublique();
resultat += variablePublique;`;

const typeModule = 
`{
    ...

    "main": "server.js",
    "type": "module",

    ...
}`;

const creerModule = 
`// Toute les variables dans ce fichier sont privée 
// au fichier
let uneVar = 'Pas accessible d\\'un autre fichier';

// Les classes ne sont pas accessibles non plus
class Test {
    // ...
}

// Les fonctions non plus
function Allo(){
    // ...
}`;

const creerExports = 
`let variablePrivee = 'Je suis privée';

let fonctionPublique = () => {
    return variablePrivee
};

export { fonctionPublique };`;

const creerRequire = 
`import { fonctionPublique } from './module.js';

console.log(fonctionPublique());`;

const export1 = 
`// module.js
let fonction = () => {
    // ... 
};

export default fonction;`;

const export1Main = 
`// main.js
import monModule from './module.js';`;

const exportN = 
`// module.js
let fonction = () => {
    // ... 
};

class Classe {
    // ...
}

let objet = {
    // ...
}

export { fonction };
export { Classe };
export { objet };`;

const exportNMain = 
`// main.js
import { fonction, Classe, objet } from './module.js';`;

export default function NJSModule() {
    return <>
        <section>
            <h2>Historique - Module closure</h2>
            <p>
                Les modules sont une façon de séparer le code en plusieurs fichiers. Dans le domaine, on appèle 
                souvent ce concept le <strong>Code Splitting</strong>. Les premiers types de modules sont apparût initialement 
                en Javascript dans les navigateur Web.
            </p>
            <p>
                Les modules dans les navigateurs Web servaient principalement à ne pas polluer l'espace des 
                variables globales puisque dans un navigateur, toutes variables créées à la racine d'un fichier 
                était considéré globale. On appelait ce type de module un <strong>Module Closure</strong> puisqu'ils 
                utilisent les propriétés des blocs de fonction (aussi appelé Closure) pour fonctionner. Voici un exemple:
            </p>
            <CodeBlock language="js">{ moduleClosure }</CodeBlock>
            <CodeBlock language="js">{ moduleClosureMain }</CodeBlock>
        </section>

        <section>
            <h2>Node.js - Module CommonJS</h2>
            <p>
                En Node.js, les modules servent à des fonctions différentes puisque par défaut, les variables en 
                Node.js sont toujours privée à leur fichier (scopé par le fichier). Bref, les modules en Node.js 
                font le contraire d'un Closure Module. Ils vont servir en fait à exposer des variables, fonctions, 
                classes ou objets à d'autres fichiers. On appelle ces modules, 
                les <strong>Module CommonJS</strong>. Voici un exemple d'utilisation:
            </p>
            <CodeBlock language="js">{ moduleCommonJs }</CodeBlock>
            <CodeBlock language="js">{ moduleCommonJsMain }</CodeBlock>
        </section>

        <section>
            <h2>Standard - Module ES6</h2>        
            <p>
                Javascript a aujourd'hui standardisé le concept de module de façon similaire à Node.js pour les 
                navigateurs Web et pour Node.js. Bien que tout récent, ces modules sont aujourd'hui le standard pour 
                les navigateurs web et Node.js puisqu'ils sont supporté par plus de 95% des environnements d'exécution 
                de Javascript. On appèle ce type de modules un <strong>Module ES6</strong>. La syntaxe ressemble à 
                ceci:
            </p>
            <CodeBlock language="js">{ moduleEC6 }</CodeBlock>
            <CodeBlock language="js">{ moduleEC6Main }</CodeBlock>
            <p>
                Comme vous pouvez le remarquer, les modules EC6 et les modules CommonJS sont très similaire. La 
                syntaxe de <IC>exports</IC> change légèrement et les modules EC6 utilisent les <IC>import</IC> au 
                lieu des <IC>require</IC> des modules CommonJS.
            </p>
            <p>
                Dans ce cours, nous utiliserons les modules ES6 puisqu'ils sont maintenant supporté par Node.js.
            </p>
        </section>

        <section>
            <h2>Créer son propre module</h2>
            <p>
                Pour utiliser les modules ES6 dans votre projet vous devez tout d'abords faire une petite modification 
                au fichier <IC>package.json</IC>. Vous devez ajouter la ligne suivante sous le <IC>"main": "server.js"</IC>:
            </p>
            <CodeBlock language="json">{ typeModule }</CodeBlock>
            <p>
                Cette ligne indique à Node.js qu'il doit utiliser les modules ES6 au lieu des modules CommonJS. Par 
                défaut, Node.js utilise encore les modules CommonJS pour des raisons de rétrocompatibilité.
            </p>
            <p>
                Par la suite, pour créer un module ES6, vous n'avez qu'à créer un nouveau fichier Javascript. 
                Un fichier Javascript sera automatiquement considéré comme un module et tout son contenu restera 
                privée à lui-même.
            </p>
            <CodeBlock language="js">{ creerModule }</CodeBlock>
            <p>
                Si vous voulez exposer une variable, une fonction ou une classe, vous devez utiliser le mot-clé <IC>export</IC>.
            </p>
            <CodeBlock language="js">{ creerExports }</CodeBlock>
            <p>
                Si vous voulez accèder à un élément exposer par <IC>export</IC> à partir d'un autre fichier, vous
                devez utiliser le mot-clé <IC>import</IC> avec le chemin vers le fichier du module.
            </p>
            <CodeBlock language="js">{ creerRequire }</CodeBlock>
        </section>

        <section>
            <h2>Type d'exportations</h2>
            <p>
                Il y a plusieurs façon de faire des exportation dans les modules ES6. Les 2 principaux sont 
                l'exportation par défaut et l'exportation d'éléments:
            </p>
            <ol>
                <li>
                    Si vous voulez exporter un seul élément (variable, fonction, classe ou objet), 
                    utilisez <IC>export default</IC>. 
                    <CodeBlock language="js">{ export1 }</CodeBlock>
                    <CodeBlock language="js">{ export1Main }</CodeBlock>
                </li>
                <li>
                    Si vous voulez exporter plusieurs éléments (variable, fonction, classe ou objet), 
                    utilisez <IC>export</IC> tout seul suivi du nom des éléments entre accolades <IC>{'{ ... }'}</IC>. 
                    <CodeBlock language="js">{ exportN }</CodeBlock>
                    <CodeBlock language="js">{ exportNMain }</CodeBlock>
                </li>
            </ol>
        </section>
    </>;
}
