import React from 'react';
import IC from '../component/InlineCode';
import CodeBlock from '../component/CodeBlock';

const configure = 
`const MemoryStore = memorystore(session);
app.use(session({
    cookie: { maxAge: 3600000 },
    name: process.env.npm_package_name,
    store: new MemoryStore({ checkPeriod: 3600000 }),
    resave: false,
    saveUninitialized: false,
    secret: process.env.SESSION_SECRET
}));`;

const addSession = 
`app.get('/une-route', (request, response) => {
    // On crée une variable "nomDeVariable"
    // dans la session
    request.session.nomDeVariable = 'valeur';
});`;

const addSessionStart = 
`app.get('/une-route', (request, response) => {
    // Si le tableau n'existe pas, on le crée
    if(!request.session.nomDeVariable){
        request.session.nomDeVariable = [];
    }
    
    // Dans tous les cas, on ajoute un nombre
    // aléatoire au tableau
    request.session.nomDeVariable.push(Math.random());
});`;

const deleteSession = 
`app.get('/une-route', (request, response) => {
    // Suppression de la variable "nomDeVariable"
    // dans la session
    delete request.session.nomDeVariable;
});`;

export default function SessionVar() {
    return <>
        <section>
            <h2>Configuration</h2>
            <p>
                Avant d'utiliser les sessions, nous devons ajouter les middlewares nécessaires et les configurer. Pour
                ce faire, ajoutez le code suivant avec l'ajout des autres middlewares:
            </p>
            <CodeBlock language="js">{ configure }</CodeBlock>
            <p>
                Voici quelques informations sur la configuration:
            </p>
            <dl>
                <dt>cookie.maxAge</dt>
                <dd>
                    Permet de définir le temps nécessaire en millisecondes avant qu'une session soit supprimé du 
                    serveur. Dans le cas ci-dessus, la session a une durée de vie de 1h. En général, à chaque 
                    utilisation de la session, celle-ci est rafraîchit automatiquement.
                </dd>

                <dt>name</dt>
                <dd>
                    C'est le nom de la session. Si vous avez plusieurs serveur Web qui sont exécuté en 
                    même temps sur votre ordinateur, ça permet aux sessions de faire la différence entre ces 
                    applications. Dans notre cas, j'utilise le nom de votre application dans le 
                    fichier <IC>package.json</IC>. Il est donc très important de le changer pour chaque projets.
                </dd>

                <dt>store</dt>
                <dd>
                    C'est l'instance de l'utilitaire que vous utiliserez pour sauvegarder vos sessions. Dans notre 
                    cas, on utilise notre <IC>MemoryStore</IC> que nous avons téléchargé précédemment.
                </dd>

                <dt>store.checkPeriod</dt>
                <dd>
                    C'est la fréquence em millisecondes à laquelle notre utilitaire de sauvegarde des sessions va 
                    regarder ses sessions pour les supprimer si elle sont trop vieille. Dans le cas ci-dessus, 
                    le <IC>MemoryStore</IC> regarde à chaque heure pour voir si les sessions sont périmées.
                </dd>

                <dt>resave</dt>
                <dd>
                    Indique si l'engin de session doit réenregistrer la session même si elle n'a pas été modifié. 
                    Par défaut, la valeur est à <IC>true</IC>, mais il est fortement recommandé de la mettre 
                    à <IC>false</IC>, ce que nous faisons ci-dessus.
                </dd>

                <dt>saveUninitialized</dt>
                <dd>
                    Indique si l'engin de session doit enregistrer les sessions même si elles sont vide. Par défaut,
                    la valeur est à <IC>true</IC>, mais il est fortement recommandé de la mettre 
                    à <IC>false</IC>, ce que nous faisons ci-dessus.
                </dd>

                <dt>secret</dt>
                <dd>
                    C'est la clé pour générer les cookies de connexion. Ça vous prends absolument une valeur ici.
                    Dans une application dont la sécurité est vraiment importante, il serait nécessaire de générer des
                    secrets différents périodiquement et de faire des rotations. Dans notre cas, nous mettrons
                    simplement le secret dans notre fichier de configuration <IC>.env</IC>.
                </dd>
            </dl>
        </section>
        
        <section>
            <h2>Variable de session</h2>
            <p>
                Une fois les sessions installées et configurées, vous pourrez accéder à la session de chaque connexion
                dans les routes que vous programmez dans la variable <IC>request.session</IC>. Cette variable est un
                objet. Vous pouvez donc y ajouter des champs sans aucun problème.
            </p>
            <CodeBlock language="js">{ addSession }</CodeBlock>
            <p>
                Si vous désirez ajouter des données pour la première fois dans votre session sur une route qui 
                est souvent utilisé par l'utilisateur, je vous suggère toutefois un code un peu différent. Dans le 
                code ci-dessous, on s'assure que la variable n'a pas déjà été créé au préalable pour être certain 
                de ne pas écraser sa valeur.
            </p>
            <CodeBlock language="js">{ addSessionStart }</CodeBlock>
            <p>
                Finalement, si vous voulez supprimer des données dans une session, vous pouvez utiliser le 
                mot-clé <IC>delete</IC> de la façon suivante:
            </p>
            <CodeBlock language="js">{ deleteSession }</CodeBlock>
        </section>
    </>
}