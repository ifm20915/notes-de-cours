import React from 'react';
import IC from '../component/InlineCode'
import Video from '../component/Video'
import CodeBlock from '../component/CodeBlock'
import DownloadBlock from '../component/DownloadBlock'

import distibue from '../resources/laboratoire-11-distribué.zip'
import solution from '../resources/laboratoire-11-solution.zip'

const messageData =
`let data = {
    // Indique si c'est l'utilisateur qui vient d'envoyer le message
    isSelf: true,

    // Indique le nom de l'utilisateur
    userName: users.get(uuid).name,

    // Indique la couleur de l'utilisateur
    color: users.get(uuid).color,

    // Le texte du message
    message: text
}`;

class Laboratoire11 extends React.Component {
    render() {
        return <>
            <section>
                <h2>Marche à suivre</h2>
                <p>
                    Compléter le projet distribué du clavardage anonyme. Voici les étapes à suivre:
                </p>
                <ol>
                    <li>
                        Dans le fichier <IC>chatroom-sse.js</IC>, compléter la fonction <IC>initStream</IC> pour 
                        qu'elle envoit un évènement sur le stream contenant l'identifiant unique de l'utilisateur.
                    </li>
                    <li>
                        Dans le fichier <IC>chatroom-sse.js</IC>, compléter la fonction <IC>sendMessage</IC> pour 
                        qu'elle valide ses 2 paramètres. Le <IC>text</IC> et le <IC>uuid</IC> ne doivent pas être vide 
                        ou nul. De plus, le <IC>uuid</IC> doit être celui d'un utilisateur existant. L'identifiant des
                        utilisateurs se retrouvent dans la <IC>Map</IC> qui se nomme <IC>users</IC>. Voir les Astuces
                        pour savoir comment faire.
                    </li>
                    <li>
                        Dans le fichier <IC>chatroom-sse.js</IC>, compléter la fonction <IC>sendMessage</IC> pour 
                        qu'elle envoit le message spécifié à tous les clients actifs. Voir les Astuces pour savoir 
                        comment boucler sur une <IC>Map</IC>. Assurez-vous d'envoyer un objet ayant les propriétés 
                        suivantes:
                        <CodeBlock language="js">{ messageData }</CodeBlock>
                    </li>
                    <li>
                        Dans le fichier <IC>server.js</IC>, ajouter le code de la route pour initialiser la connexion 
                        avec le middleware <IC>chatroomSse</IC> que nous avons créé.
                    </li>
                    <li>
                        Dans le fichier <IC>server.js</IC>, ajouter le code de la route pour publier un message à tous 
                        les utilisateurs actifs avec le middleware <IC>chatroomSse</IC> que nous avons créé.
                    </li>
                    <li>
                        Dans le fichier <IC>main.js</IC>, compléter la fonction <IC>init</IC> pour qu'elle initialise 
                        une connexion avec le serveur à l'aide d'un <IC>EventSource</IC>. 
                        <ol>
                            <li>
                                Ajouter un listeners à l'<IC>EventSource</IC> pour recevoir l'identifiant de 
                                l'utilisateur. Vous devez stocker cet identifiant dans la variable <IC>uuid</IC>.
                            </li>
                            <li>
                                Ajouter un listeners à l'<IC>EventSource</IC> pour recevoir les messages des 
                                utilisateurs envoyés par le serveur. Vous devez ajouter ces messages dans l'interface 
                                graphique. Vous pouvez utiliser la fonction <IC>newMessage</IC> ajouter le message 
                                dans l'interface graphique. Voir les astuces pour garder la barre de défilement en bas 
                                de la zone de clavardage.
                            </li>
                        </ol>
                    </li>
                    <li>
                        Dans le fichier <IC>main.js</IC>, compléter la fonction <IC>sendMessage</IC> pour qu'elle 
                        envoit le message spécifié dans le textbox au serveur. N'oubliez pas d'envoyer 
                        le <IC>uuid</IC> dans la requête pour que le serveur puisse valider le message. Si le serveur 
                        répond positivement à la requête, vous pouvez effacer le textbox et remettre le focus dessus.
                    </li>
                </ol>
            </section>

            <section>
                <h2>Astuces</h2>
                <ul>
                    <li>
                        Dans ce projet, nous utilisons une <IC>Map</IC> contenant tous les utilisateurs. La clé 
                        utilisé pour chaque utilisateur est un identifiant unique (uuid). La valeur utilisé est un 
                        objet de la classe <IC>User</IC> qui crée un utilisateur aléatoire à partir d'une connexion.
                        Le uuid (clé) et l'utilisateur (valeur) sont générés dans la fonction <IC>initStream</IC>.
                    </li>
                    <li>
                        Vous pouvez regarder si la <IC>Map</IC> contient une clé de la façon suivante:
                        <CodeBlock language="js">{ 'if(variableMap.has(cle)){ // ...' }</CodeBlock>
                        La valeur retourné par la fonction <IC>has</IC> est un booléen indiquant si la clé est dans 
                        la <IC>Map</IC>.
                    </li>
                    <li>
                        Vous pouvez boucler sur une <IC>Map</IC> de la façon suivante:
                        <CodeBlock language="js">{ 'for(let [cle, valeur] of variableMap){ // ...' }</CodeBlock>
                        La variable <IC>cle</IC> et <IC>valeur</IC> contiendront respectivement la clé et la valeur 
                        de chaque élément dans la <IC>Map</IC>.
                    </li>
                    <li>
                        Pour forcer une barre de défilement en bas, vous pouvez utiliser le code suivant:
                        <CodeBlock language="js">{ 'conteneur.scrollTo(0, conteneur.scrollHeight)' }</CodeBlock>
                    </li>
                </ul>
            </section>

            <section>
                <h2>Vidéo</h2>
                <Video title="Présentation - Laboratoire 11" src="https://www.youtube.com/embed/nbo3Wj5j-ZE" />
            </section>

            <section>
                <h2>Solution</h2>
                <DownloadBlock>
                    <DownloadBlock.File path={ distibue } name="distribué.zip"></DownloadBlock.File>
                    <DownloadBlock.File path={ solution } name="solution.zip"></DownloadBlock.File>
                </DownloadBlock>
            </section>
        </>;
    }
}

export default Laboratoire11;
