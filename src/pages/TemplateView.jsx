import React from 'react';
import IC from '../component/InlineCode';
import DownloadBlock from '../component/DownloadBlock';

import gabarit from '../resources/gabarit-vue.zip';

export default function TemplateView() {
    return <>
        <section>
            <h2>Guide d'utilisation</h2>
            <p>
                Pour utiliser ce gabarit, veuillez suivre les étapes suivates:
            </p>
            <ol>
                <li>Télécharger le gabarit</li>
                <li>Décompresser le gabarit</li>
                <li>Changer le nom du dossier du projet</li>
                <li>
                    Ouvrir le fichier <IC>package.json</IC>
                    <ol>
                        <li>Modifier le <IC>name</IC> du package</li>
                        <li>Modifier la <IC>description</IC> du package</li>
                        <li>Modifier l' <IC>author</IC> du package</li>
                    </ol>
                </li>
                <li>
                    Ouvrir un terminal dans le dossier du gabarit
                    <ol>
                        <li>Lancer la commande <IC>npm install</IC></li>
                        <li>Lancer la commande <IC>npm start</IC></li>
                    </ol>
                </li>
            </ol>
            <p>
                Ce gabarit contient un projet express (presque) vide utilisant un middleware de génération de HTML
                serveur ainsi que plusieurs autres middlewares et outils pour créer un serveur web complexe. Il
                utilise les concepts de Handlebars pour créer une page simple que vous pouvez modifier.
            </p>
        </section>

        <section>
            <h2>Téléchargement</h2>
            <DownloadBlock>
                <DownloadBlock.File path={ gabarit } name="gabarit-vue.zip"></DownloadBlock.File>
            </DownloadBlock>
        </section>
    </>
}